import puppeteer from "puppeteer"
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
  })
  const page = await browser.newPage()
  await page.setCacheEnabled(false)
  await page.setCookie({
    name: "TEST_SCENE",
    url: "http://127.0.0.1:8080",
    value: "BasicTest",
  })
  await page.goto("http://127.0.0.1:8080/", {
      waitUntil: "networkidle0",
  })
  await page.keyboard.press("KeyS", {
    delay: 500,
  })
  await page.screenshot({path: "test/screenshots/generated/BasicTest/1.png", fullPage: true})
  await page.keyboard.press("KeyA", {
    delay: 200,
  })
  await page.keyboard.press("KeyS", {
    delay: 2300,
  })
  await page.keyboard.press("KeyD", {
    delay: 500,
  })
  await page.keyboard.press("KeyE", {
    delay: 100,
  })
  await page.screenshot({path: "test/screenshots/generated/BasicTest/2.png", fullPage: true})
  await page.keyboard.press("Space")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/3.png", fullPage: true})
  await page.keyboard.press("Space")
  await page.keyboard.press("Space")
  await page.keyboard.press("Space")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/4.png", fullPage: true})
  await page.keyboard.press("KeyS")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/5.png", fullPage: true})
  await page.keyboard.press("KeyW")
  await page.keyboard.press("Space")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/6.png", fullPage: true})
  await page.keyboard.press("Space")
  await page.keyboard.press("KeyS")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/7.png", fullPage: true})
  await page.keyboard.press("Space")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/8.png", fullPage: true})
  await page.keyboard.press("Space")
  await page.keyboard.press("KeyS")
  await page.keyboard.press("KeyS")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/9.png", fullPage: true})
  await page.keyboard.press("Space")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/10.png", fullPage: true})
  await page.keyboard.press("Space")
  await page.screenshot({path: "test/screenshots/generated/BasicTest/11.png", fullPage: true})
  await browser.close()
})()
