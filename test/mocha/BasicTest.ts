import { assert } from "chai"
import { execSync } from "child_process"
import * as fs from "fs"
import { describe  } from "mocha"
import { step } from "mocha-steps"
import path from "path"
import Pixelmatch from "pixelmatch"
import {PNG} from "pngjs"

describe("BasicTest", () => {
    step("Purging generated screenshots", () => {
        const pathToScreens = path.join(__dirname, "../screenshots/generated/BasicTest")
        fs.readdir(pathToScreens, (errDir, files: string[]) => {
            if (errDir) {
                throw errDir
            } else {
                let i = 0
                files.forEach((file) => {
                    if (file !== ".gitkeep") {

                    fs.unlink(path.join(pathToScreens, file), (errUnlink) => {
                        if (errUnlink) {
                            throw errUnlink
                        }
                        i++
                        if (i === files.length - 1) {
                            assert.equal(i, files.length - 1)
                        }
                    })
                    }
                })
            }
        })
    })
    step("Running tests and waiting for screenshots", () => {
        const pathToTest = path.join(__dirname, "../puppeteer/BasicTest/Test.ts")
        execSync(`node -r ts-node/register -r tsconfig-paths/register ${pathToTest}`)
        assert.equal(1, 1)
    }).timeout(60000)
    step("Comparing known screenshots with new", () => {
        const pathToGeneratedScreens = path.join(__dirname, "../screenshots/generated/BasicTest")
        const pathToKnownScreens = path.join(__dirname, "../screenshots/excepted/BasicTest")
        const knownScreens: {[index: string]: PNG} = {}
        const generatedScreens: {[index: string]: PNG} = {}
        const exceptedFiles = fs.readdirSync(pathToKnownScreens)
        exceptedFiles.forEach((file) => {
            const buff = fs.readFileSync(path.join(pathToKnownScreens, file))
            knownScreens[file] = PNG.sync.read(buff)
        })

        const generatedFiles = fs.readdirSync(pathToGeneratedScreens)
        generatedFiles.forEach((file) => {
            if (file !== ".gitkeep") {
                const buff = fs.readFileSync(path.join(pathToGeneratedScreens, file))
                generatedScreens[file] =  PNG.sync.read(buff)
            }
        })

        const {width, height} = knownScreens[Object.keys(knownScreens)[0]]
        Object.keys(knownScreens).forEach((key) => {
            const diff = new PNG({width, height})
            const diffPixels = Pixelmatch(knownScreens[key].data, generatedScreens[key].data,
                diff.data, width, height, {
                    threshold: 0.35,
                })
            assert.equal(diffPixels <= 350, true, `Screenshot ${key} isn't equal! (Diffrent pixels: ${diffPixels})`)
        })

    })
})
