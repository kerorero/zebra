import { IDialogManager } from "./IDialogManager"

export interface IDialogResponseFunctionExecutable {
    execute: (args: string[] | null, dialogManager: IDialogManager) => void
}
