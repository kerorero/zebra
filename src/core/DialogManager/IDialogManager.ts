import { IDialog } from "core/Dialog/IDialog"

export interface IDialogManager {
    title: string
    displayDialogs: (dialogs: IDialog[]) => void
    closeActiveDialog: () => void,
    allKnownDialogs: {[name: string]: IDialog}
}
