import { IDialogManager } from "../IDialogManager"
import { IDialogResponseFunctionExecutable } from "../IDialogResponseFunctionExecutable"

export class DialogResponseCancelFunction implements IDialogResponseFunctionExecutable {
    private _stage: PIXI.Container = null
    constructor(stage: PIXI.Container) {
        this._stage = stage

    }
    public execute(args: string[] | null, dialogManager: IDialogManager) {
        dialogManager.closeActiveDialog()
        this._stage.emit("onPlayerEndDialogue")
    }
}
