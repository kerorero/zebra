import { IDialogManager } from "../IDialogManager"
import { IDialogResponseFunctionExecutable } from "../IDialogResponseFunctionExecutable"

export class DialogResponseBackToDialogueFunction implements IDialogResponseFunctionExecutable {
    public execute(args: string[] | null, dialogManager: IDialogManager) {
        if (args && args.length > 0) {
            Object.keys(dialogManager.allKnownDialogs).forEach((uniqueName: string) => {
                if (args[0] === uniqueName) {
                    dialogManager.displayDialogs([dialogManager.allKnownDialogs[uniqueName]])
                }
            })
        }
    }
}
