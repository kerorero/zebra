import { ICamera } from "core/Camera/ICamera"
import { IStaticCameraPointCalculator } from "core/Camera/IStaticCameraPointCalculator"
import { StaticCameraPoint } from "core/Camera/StaticCameraPoint"
import { IDialog } from "core/Dialog/IDialog"
import { IDialogQuestion } from "core/Dialog/IDialogQuestion"
import { IDialogResponse } from "core/Dialog/IDialogResponse"
import { IDialogResponseFunction } from "core/Dialog/IDialogResponseFunction"
import { IPixiTextFactory } from "core/PixiFactories/IPixiTextFactory"
import { ScreenPositionFactory } from "core/WorldPositionFactory/ScreenPositionFactory"
import { IDialogManager } from "./IDialogManager"
import { IDialogResponseFunctionExecutable } from "./IDialogResponseFunctionExecutable"
import { SelectDialogueOptionDirection } from "./SelectDialogueOptionDirection"

export class DialogManager implements IDialogManager {

    get allKnownDialogs() {
        return this._allKnownDialogs
    }
    set title(title: string) {
        this._title = title
    }
    get title() {
        return this._title
    }

    set camera(camera: ICamera) {
        if (!this._camera) {
            this._camera = camera
        }
    }
    private _stage: PIXI.Container = null
    private _dialogBackgroundSprite: PIXI.Sprite = null
    private _staticCameraPointCalculator: IStaticCameraPointCalculator = null
    private _pixiTextFactory: IPixiTextFactory = null
    private _screenPositionFactory: ScreenPositionFactory = null
    private _textsInDialog: PIXI.Text[] = []
    private _selectedOptionInDialog: number = 0
    private _symbolToMarkSelectedOption: string = "--->"
    private _symbolSize: number = 12
    private _dialogResponses: {[index: number]: IDialogResponse[]} = {}
    private _titleText: PIXI.Text = null
    private _actualResponseText: PIXI.Text = null
    private _actualDisplayedResponse: number = 0
    private _lastDialogs: IDialog[] = null
    private _allKnownDialogs: {[name: string]: IDialog} = {}
    private _knownResponsesFunctions: {[name: string]: IDialogResponseFunctionExecutable}
    private _title: string = null
    private _camera: ICamera = null

    constructor(
        stage: PIXI.Container,
        camera: ICamera,
        staticCameraPointCalculator: IStaticCameraPointCalculator,
        pixiTextFactory: IPixiTextFactory,
        screenPositionFactory: ScreenPositionFactory,
        dialogResponseFunctions: {[name: string]: IDialogResponseFunctionExecutable},
    ) {
        this._stage = stage
        this._camera = camera
        this._pixiTextFactory = pixiTextFactory
        this._staticCameraPointCalculator = staticCameraPointCalculator
        this._screenPositionFactory = screenPositionFactory
        this._knownResponsesFunctions = dialogResponseFunctions
        this._stage.on("onResourcesLoad", this.onResourcesLoad, this)
        this._stage.on("onPlayerSelectDialogueQuestionOption", this.onPlayerSelectDialogueQuestionOption, this)
        this._stage.on("onPlayerSelectActiveDialogueOption", this.onPlayerSelectActiveDialogueOption, this)
        this._stage.on("onPlayerSelectNextDialogueResponse", this.onPlayerSelectNextDialogueResponse, this)
    }

    public onResourcesLoad(resources) {
        this._dialogBackgroundSprite = resources.uiDialogBackground
        this._dialogBackgroundSprite.scale.x = 2.2
        this._camera.attachObjectToCamera(this._dialogBackgroundSprite,
            this._staticCameraPointCalculator.calculateObjectScreenPosition(
                this._dialogBackgroundSprite, StaticCameraPoint.DOWN_CENTER,
            ),
        )
    }

    public onPlayerSelectActiveDialogueOption() {
        this._displayDialogResponses()
    }

    public displayDialogs(dialogs: IDialog[]) {
        this._stage.addChild(this._dialogBackgroundSprite)
        // tslint:disable-next-line: no-console
        console.log("what is with you")
        this._selectedOptionInDialog = 0
        this._lastDialogs = dialogs
        this._titleText = this._pixiTextFactory.create(this._title,
            {fontFamily: "Arial", fontSize: 16, fill : 0xFFFFFF},
        )
        this._titleText.visible = false
        this._titleText.style.fontWeight = "bold"
        this._titleText.scale.x = 1 / this._dialogBackgroundSprite.scale.x
        this._titleText.position = this._screenPositionFactory.create(
            5, 5,
        )
        this._dialogBackgroundSprite.addChild(this._titleText)
        this._textsInDialog = []
        dialogs.forEach((dialog: IDialog) => {
            if (dialog.uniqueName) {
                this._allKnownDialogs[dialog.uniqueName] = dialog
            }
            this._addDialogOption(dialog)
        })
    }

    public onPlayerSelectDialogueQuestionOption(direction: SelectDialogueOptionDirection) {
        const availableOptions: number = this._textsInDialog.length - 1
        if (this._selectedOptionInDialog > 0 && direction === SelectDialogueOptionDirection.UP) {
            this._selectedOptionInDialog--
        } else if (this._selectedOptionInDialog < availableOptions &&
            direction === SelectDialogueOptionDirection.DOWN) {
            this._selectedOptionInDialog++
        }
        this._colorSelectedOpton()
    }

    public onPlayerSelectNextDialogueResponse() {
        this._actualDisplayedResponse++
        if (this._actualDisplayedResponse > this._dialogResponses[this._selectedOptionInDialog].length - 1) {
            this._dialogBackgroundSprite.removeChild(this._actualResponseText)
            this._dialogBackgroundSprite.removeChild(this._titleText)
            this._actualResponseText = null
            const functions: IDialogResponseFunction[] | null = this._dialogResponses[
                this._selectedOptionInDialog
            ][this._actualDisplayedResponse - 1].functions
            if (functions && functions.length > 0) {
                functions.forEach((func: IDialogResponseFunction) => {
                    if (this._knownResponsesFunctions[func.name]) {
                        this._knownResponsesFunctions[func.name].execute(func.extraArgs, this)
                    }
                })
            } else {
                const nextDialogs: IDialog[] = (this._dialogResponses[
                    this._selectedOptionInDialog
                ][this._actualDisplayedResponse - 1].dialog) ?
                [this._dialogResponses[
                    this._selectedOptionInDialog
                ][this._actualDisplayedResponse - 1].dialog] : this._lastDialogs
                this.displayDialogs(nextDialogs)
            }
            this._stage.emit("onPlayerEndDialogueResponses")
        } else {
            this._displayDialogResponse(
                this._dialogResponses[this._selectedOptionInDialog][this._actualDisplayedResponse],
            )
        }
    }

    public closeActiveDialog() {
        this._stage.removeChild(this._dialogBackgroundSprite)
        this._lastDialogs = null
    }

    private _displayDialogResponses() {
        this._textsInDialog.forEach((textInDialog: PIXI.Text) => {
            this._dialogBackgroundSprite.removeChild(textInDialog)
        })
        this._titleText.visible = true
        this._actualDisplayedResponse = 0
        this._displayDialogResponse(this._dialogResponses[this._selectedOptionInDialog][this._actualDisplayedResponse])
    }

    private _displayDialogResponse(response: IDialogResponse) {
        if (this._actualResponseText) {
            this._dialogBackgroundSprite.removeChild(this._actualResponseText)
        }
        this._actualResponseText = this._pixiTextFactory.create(
            response.response,  {fontFamily: "Arial", fontSize: 14, fill : 0xFFFFFF},
        )
        this._actualResponseText.scale.x = 1 / this._dialogBackgroundSprite.scale.x
        this._actualResponseText.position = this._screenPositionFactory.create(
            5, 27,
        )
        this._dialogBackgroundSprite.addChild(this._actualResponseText)
    }

    private _addDialogOption(dialog: IDialog) {
        let y: number = 0
        dialog.questions.forEach((dialogQuestion: IDialogQuestion, index: number) => {
            const dialogText: PIXI.Text = this._pixiTextFactory.create(dialogQuestion.question,
                {fontFamily: "Arial", fontSize: 14, fill : 0xFFFFFF},
            )
            dialogText.position = this._screenPositionFactory.create(
                this._symbolSize, y,
            )
            dialogText.scale.x = (1 / this._dialogBackgroundSprite.scale.x)
            this._textsInDialog.push(dialogText)
            this._dialogResponses[index] = dialogQuestion.responses
            this._dialogBackgroundSprite.addChild(dialogText)
            y += 20
        })
        this._colorSelectedOpton()
    }

    private _colorSelectedOpton() {
        this._textsInDialog.forEach((textObject: PIXI.Text) => {
            textObject.style.fontWeight = "lighter"
            textObject.text = textObject.text.replace(this._symbolToMarkSelectedOption, "").trimLeft()
            textObject.x = this._symbolSize
        })
        this._textsInDialog[this._selectedOptionInDialog].style.fontWeight = "bold"
        this._textsInDialog[this._selectedOptionInDialog].text =
            `${this._symbolToMarkSelectedOption} ${this._textsInDialog[this._selectedOptionInDialog].text}`
        this._textsInDialog[this._selectedOptionInDialog].x = 0

    }
}
