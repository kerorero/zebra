import { IAppSetting } from "core/AppSetting/IAppSetting"
import { IResourcesLoader } from "core/ResourcesLoader/IResourcesLoader"
import { ISceneEditor } from "core/SceneEditor/ISceneEditor"
import $ from "jquery"
import * as PIXI from "pixi.js"
import { IPixiApplicationFactory } from "../PixiFactories/IPixiApplicationFactory"

export class App {
   private _app: PIXI.Application = null
   private _pixiApplicationFactory: IPixiApplicationFactory = null
   private _appSetting: IAppSetting = null
   private _resourcesLoader: IResourcesLoader = null
   private _renderer: PIXI.Renderer = null
   get renderer(): PIXI.Renderer {
      return this._renderer
   }

   private _stage: PIXI.Container = null
   get stage(): PIXI.Container {
      return this._stage
   }

   constructor(
   pixiApplicationFactory: IPixiApplicationFactory,
   appSetting: IAppSetting,
   resourcesLoader: IResourcesLoader) {
      this._pixiApplicationFactory = pixiApplicationFactory
      this._appSetting = appSetting
      this._resourcesLoader = resourcesLoader
   }

   public prepare() {
      this._app = this._pixiApplicationFactory.create({
         backgroundColor: this._appSetting.mapBackgroundColor,
         height: this._appSetting.worldHeight,
         resolution: window.devicePixelRatio || 1,
         width: this._appSetting.worldWidth,
      })
      this._app.stage.sortableChildren = true
      this._stage = this._app.stage
      this._renderer = this._app.renderer
   }

   public init() {
      this._resourcesLoader.load((loader, resources) => {
         this._app.stage.emit("onResourcesLoad", resources)
         $(this._appSetting.mainDiv).append(this._app.view)

         this._app.ticker.add((delta: number) => {
            this._app.stage.emit("onUpdate", delta)
         })
         this._renderer.plugins.interaction.on("mousemove",
         (event: PIXI.interaction.InteractionEvent) => {
            this._app.stage.emit("onMouseMove", event)
         })
         this._renderer.plugins.interaction.on("mouseup",
         (event: PIXI.interaction.InteractionEvent) => {
            this._app.stage.emit("onMouseUp", event)
         })
         this._renderer.plugins.interaction.on("mousedown",
         (event: PIXI.interaction.InteractionEvent) => {
            this._app.stage.emit("onMouseDown", event)
         })
         this._renderer.plugins.interaction.on("rightdown",
         (event: PIXI.interaction.InteractionEvent) => {
            this._app.stage.emit("onMouseRightButtonDown", event)
         })
         this._renderer.plugins.interaction.on("rightup",
         (event: PIXI.interaction.InteractionEvent) => {
            this._app.stage.emit("onMouseRightButtonUp", event)
         })
         window.oncontextmenu = (event: MouseEvent) => {
            event.preventDefault()
            return false
         }
         window.onkeydown = (event: KeyboardEvent) => {
            this._app.stage.emit("onKeyDown", event)
         }
         window.onkeyup = (event: KeyboardEvent) => {
            this._app.stage.emit("onKeyUp", event)
         }
      })
   }
}
