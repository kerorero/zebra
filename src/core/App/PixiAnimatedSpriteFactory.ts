import * as PIXI from "pixi.js"
import { IPixiAnimatedSpriteFactory } from "../PixiFactories/IPixiAnimatedSpriteFactory"

export class PixiAnimatedSpriteFactory implements IPixiAnimatedSpriteFactory {
   public create(
      textures: PIXI.Texture[] | PIXI.AnimatedSprite.FrameObject[],
      autoUpdate: boolean = true,
   ) {
      return new PIXI.AnimatedSprite(textures, autoUpdate)
   }
}
