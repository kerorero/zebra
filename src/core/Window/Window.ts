export class Window {
   protected _stage: PIXI.Container = null

   get name(): string {
      return this._name
   }
   protected _name: string = ""

   get isShowed(): boolean {
      return this._isShowed
   }
   protected _isShowed: boolean = false

   constructor(stage: PIXI.Container, name?: string) {
      this._stage = stage
      this._name = name
      this._stage.on("onWindowOpen", this.onWindowOpen, this)
      this._stage.on("onWindowClose", this.onWindowClose, this)
   }

   public onWindowOpen(name: string) {
      if (name !== this._name) {
         return
      }
      this._isShowed = true
   }

   public onWindowClose(name: string) {
      if (name !== this._name) {
         return
      }
      this._isShowed = false
   }
}
