import { IAppSetting } from "core/AppSetting/IAppSetting"
import { IPixiGraphicsFactory } from "core/PixiFactories/IPixiGraphicsFactory"
import { IPixiSpriteFactory } from "core/PixiFactories/IPixiSpriteFactory"
import { ScreenPosition } from "core/WorldPosition/ScreenPosition"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { WorldPositionFactory } from "core/WorldPositionFactory/WorldPositionFactory"
import { Window } from "./Window"

export class SceneEditorChooseObjectWindow extends Window {
   protected _stage: PIXI.Container = null
   protected _name: string = ""
   private _pixiSpriteFactory: IPixiSpriteFactory = null
   private _pixiGraphicsFactory: IPixiGraphicsFactory = null
   private _appSetting: IAppSetting = null
   private _graphics: PIXI.Graphics = null
   private _resources = null
   private _renderer: PIXI.Renderer = null
   private _spritesPositions: {[key: string]: number} = {}
   private _screenPositionFactory: WorldPositionFactory = null

   constructor(
      stage: PIXI.Container,
      pixiSpriteFactory: IPixiSpriteFactory,
      pixiGraphicsFactory: IPixiGraphicsFactory,
      appSetting: IAppSetting,
      renderer: PIXI.Renderer,
      screenPositionFactory: WorldPositionFactory,
   ) {
      super(stage, "SceneEditorChooseObject")
      this._stage = stage
      this._pixiGraphicsFactory = pixiGraphicsFactory
      this._pixiSpriteFactory = pixiSpriteFactory
      this._appSetting = appSetting
      this._renderer = renderer
      this._screenPositionFactory = screenPositionFactory
      this._stage.on("onResourcesLoad", this.onResourcesLoad, this)
      this._stage.on("onScreenClick", this.onScreenClick, this)
   }

   public onResourcesLoad(resources) {
      this._resources = resources
      this._graphics = this._pixiGraphicsFactory.create()
      this._graphics.beginFill(0xFFFFFF)
      this._graphics.lineStyle(0, 0xFFFFFF)
      this._graphics.drawRect(0, 0, 832, 496)
      this._graphics.pivot.x = 416
      this._graphics.pivot.y = 248
      const screenCenterPos: ScreenPosition = this._screenPositionFactory.create(
         this._renderer.width / 2, this._renderer.height / 2,
      )
      const convertedPos: WorldPosition = this._convertPositionToWorldPosition(screenCenterPos)
      this._graphics.x = convertedPos.x
      this._graphics.y = convertedPos.y - 8

      const rows = Math.floor(this._graphics.height / this._appSetting.mapSize)
      const cols = Math.floor(this._graphics.width / this._appSetting.mapSize)
      let objectId = 0
      for (let row = 0; row < rows; row++) {
         for (let col = 0; col < cols; col++) {
            if (this._resources[`objectId${objectId}`] && this._resources[`objectId${objectId}`].texture) {
               const spriteForTexture = this._pixiSpriteFactory.create(this._resources[`objectId${objectId}`].texture)
               spriteForTexture.x = col * this._appSetting.mapSize
               spriteForTexture.y = row * this._appSetting.mapSize
               const globalPoint: PIXI.IPoint = this._graphics.toGlobal(spriteForTexture.position)
               globalPoint.x = Math.floor(globalPoint.x / this._appSetting.mapSize) * this._appSetting.mapSize
               globalPoint.y = Math.floor(globalPoint.y / this._appSetting.mapSize) * this._appSetting.mapSize
               const screenPosition: ScreenPosition = this._screenPositionFactory.create(
                  globalPoint.x,
                  globalPoint.y,
               )
               this._spritesPositions[screenPosition.toString()] = objectId
               this._graphics.addChild(spriteForTexture)
               objectId++
            }
         }
      }
   }

   public onScreenClick(screenPosition: WorldPosition) {
      if (this._isShowed) {
         screenPosition = this._convertPositionToWorldPosition(screenPosition)
         const objectId: number = this._spritesPositions[screenPosition.toString()]
         if (objectId !== undefined && objectId !== null) {
            this._stage.emit("onPlayerSelectObjectFromWindow", objectId)
            this._stage.emit("onWindowClose", "SceneEditorChooseObject")
         }
      }
   }

   public onWindowOpen(name: string) {
      if (this._isShowed) {
         return this.onWindowClose(name)
      }
      super.onWindowOpen(name)
      this._stage.addChild(this._graphics)
   }

   public onWindowClose(name: string) {
      super.onWindowClose(name)
      this._stage.removeChild(this._graphics)
   }

   private _convertPositionToWorldPosition(position: WorldPosition) {
      position.x = Math.floor(position.x / this._appSetting.mapSize) * this._appSetting.mapSize
      position.y = Math.floor(position.y / this._appSetting.mapSize) * this._appSetting.mapSize
      return position
   }
}
