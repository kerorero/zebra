export interface IResourceLoaderUIConfig {
   name: string,
   fileName: string
}
