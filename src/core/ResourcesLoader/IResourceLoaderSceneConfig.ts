export interface IResourceLoaderSceneConfig {
   path: string,
   name: string,
}
