import * as PIXI from "pixi.js"
export interface IResourcesLoader {
   load: (func: (loader, res) => void) => void
}
