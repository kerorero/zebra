import { IResourceLoaderAnimationConfig } from "./IResourceLoaderAnimationConfig"
import { IResourceLoaderNPCConfig } from "./IResourceLoaderNPCConfig"
import { IResourceLoaderObjectConfig } from "./IResourceLoaderObjectConfig"
import { IResourceLoaderSceneConfig } from "./IResourceLoaderSceneConfig"
import { IResourceLoaderUIConfig } from "./IResourceLoaderUIConfig"

export interface IResourceLoaderConfig {
   objects: IResourceLoaderObjectConfig[],
   animations: IResourceLoaderAnimationConfig[],
   scenes: IResourceLoaderSceneConfig[],
   npcs: IResourceLoaderNPCConfig[],
   ui: IResourceLoaderUIConfig[],
}
