import { INPCConfig } from "core/NPCManager/INPCConfig"
import { INPCManager } from "core/NPCManager/INPCManager"
import { IAnimatedSpriteManager } from "core/ObjectManagers/IAnimatedSpriteManager"
import { ISpriteManager } from "core/ObjectManagers/ISpriteManager"
import { ISpritesheetManager } from "core/ObjectManagers/ISpritesheetManager"
import $ from "jquery"
import * as PIXI from "pixi.js"
import { IResourceLoaderAnimationConfig } from "./IResourceLoaderAnimationConfig"
import { IResourceLoaderConfig } from "./IResourceLoaderConfig"
import { IResourceLoaderNPCConfig } from "./IResourceLoaderNPCConfig"
import { IResourceLoaderObjectConfig } from "./IResourceLoaderObjectConfig"
import { IResourceLoaderSceneConfig } from "./IResourceLoaderSceneConfig"
import { IResourceLoaderUIConfig } from "./IResourceLoaderUIConfig"
import { IResourcesLoader } from "./IResourcesLoader"

export class ResourcesLoader implements IResourcesLoader {
   private _loader: PIXI.Loader = null
   private _spritesheetManager: ISpritesheetManager = null
   private _animatedSpriteManager: IAnimatedSpriteManager = null
   private _spriteManager: ISpriteManager = null
   private _npcManager: INPCManager = null

   constructor(
      spritesheetManager: ISpritesheetManager,
      animatedSpriteManager: IAnimatedSpriteManager,
      spriteManager: ISpriteManager,
      npcManager: INPCManager,
   ) {
      this._loader = PIXI.Loader.shared
      this._spritesheetManager = spritesheetManager
      this._animatedSpriteManager = animatedSpriteManager
      this._spriteManager = spriteManager
      this._npcManager = npcManager
   }

   public load(func: (loader, res) => void) {
      this._loader.add("assetsLoadConfig", "assets/config.json")
      this._loader.load((firstLoader, firstRes) => {
         const resourceLoaderConfig: IResourceLoaderConfig = $.extend({}, firstRes.assetsLoadConfig.data)
         const loadedAssets = {}
         let i = 0
         resourceLoaderConfig.objects.forEach((resourceLoaderObjectConfig: IResourceLoaderObjectConfig) => {
            const objectId = `__unknown${i}Object`
            this._loader.add(objectId, `assets/${resourceLoaderObjectConfig.path}`)
            loadedAssets[objectId] = resourceLoaderObjectConfig
            i++
         })

         i = 0
         resourceLoaderConfig.npcs.forEach((resourceLoaderNpcConfig: IResourceLoaderNPCConfig) => {
            const npcId = `__unknown${i}NPC`
            this._loader.add(npcId, `assets/npcs/${resourceLoaderNpcConfig.name}/config.json`)
            loadedAssets[npcId] = resourceLoaderNpcConfig
            i++
         })

         i = 0
         resourceLoaderConfig.ui.forEach((resourceLoaderUiConfig: IResourceLoaderUIConfig) => {
            const uiId = `__unknown${i}UI`
            this._loader.add(uiId, `assets/ui/${resourceLoaderUiConfig.fileName}`)
            loadedAssets[uiId] = resourceLoaderUiConfig
            i++
         })

         i = 0
         resourceLoaderConfig.animations.forEach((resourceLoaderAnimationConfig: IResourceLoaderAnimationConfig) => {
            let animationId = `__unknown${i}Animation`
            loadedAssets[animationId] = resourceLoaderAnimationConfig
            if (Array.isArray(resourceLoaderAnimationConfig.path)) {
               let frame = 0
               this._loader.add(animationId, `assets/${resourceLoaderAnimationConfig.path[0]}`)
               resourceLoaderAnimationConfig.path.forEach((path) => {
                  animationId = `frameForAnimation${resourceLoaderAnimationConfig.name}${frame}`
                  this._loader.add(animationId, `assets/${path}`)
                  frame++
               })
            } else {
               this._loader.add(animationId, `assets/${resourceLoaderAnimationConfig.path}`)
            }
            i++
         })

         resourceLoaderConfig.scenes.forEach((resourceLoaderSceneConfig: IResourceLoaderSceneConfig) => {
            const sceneId = `scene${resourceLoaderSceneConfig.name}`
            this._loader.add(sceneId, `assets/${resourceLoaderSceneConfig.path}`)
         })

         type NotLoaderResource<T> = T extends PIXI.LoaderResource ? never : T
         this._loader.load((loader, res: {[index: string]: NotLoaderResource<any>}) => {
            let spritesheetsId = 0
            let totalObjects = 0
            Object.keys(res).forEach((unknownObjectId) => {
               if (unknownObjectId.startsWith("__unknown")) {
                  if (unknownObjectId.endsWith("Object")) {
                     const resourceLoaderObjectConfig: IResourceLoaderObjectConfig = loadedAssets[unknownObjectId]
                     if (resourceLoaderObjectConfig.isSpritesheet) {
                        res[`spritesheet${spritesheetsId}`] = res[unknownObjectId]
                        this._spritesheetManager.spritesheetId = spritesheetsId.toString()
                        this._spritesheetManager.objectSize = resourceLoaderObjectConfig.objectSize
                        this._spritesheetManager.extraSpace = resourceLoaderObjectConfig.extraSpace
                        this._spritesheetManager.scale = resourceLoaderObjectConfig.scale
                        const results: any[] = this._spritesheetManager.load(res, totalObjects)
                        res = results[0]
                        totalObjects += results[1]
                        spritesheetsId++
                     } else {
                        totalObjects++
                        res[`object${totalObjects}`] = res[unknownObjectId]
                     }
                  } else if (unknownObjectId.endsWith("Animation")) {
                     const resourceLoaderAnimationConfig: IResourceLoaderAnimationConfig = loadedAssets[unknownObjectId]
                     if (!resourceLoaderAnimationConfig.isSpritesheet) {
                        this._animatedSpriteManager.animationName = resourceLoaderAnimationConfig.name
                        res = this._animatedSpriteManager.load(res, resourceLoaderAnimationConfig.frames)
                     }
                  } else if (unknownObjectId.endsWith("NPC")) {
                     const resourceLoaderNpc: IResourceLoaderNPCConfig = loadedAssets[unknownObjectId]
                     const npcConfig: INPCConfig = res[unknownObjectId].data
                     res[`npc${resourceLoaderNpc.name}`] = this._npcManager.load(npcConfig)
                  } else if (unknownObjectId.endsWith("UI")) {
                     const resourceLoaderUiConfig: IResourceLoaderUIConfig = loadedAssets[unknownObjectId]
                     res[`ui${resourceLoaderUiConfig.name}`] =
                        this._spriteManager.load(res[unknownObjectId].texture)
                  }

               }
            })
            func(loader, res)
         })
      })
   }
}
