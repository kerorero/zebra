export interface IResourceLoaderAnimationConfig {
   path: string | string[]
   isSpritesheet: boolean
   name: string,
   frames: number
}
