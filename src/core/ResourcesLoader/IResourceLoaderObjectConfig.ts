export interface IResourceLoaderObjectConfig {
   path: string
   isSpritesheet: boolean,
   objectSize: number
   extraSpace: number
   scale: number
}
