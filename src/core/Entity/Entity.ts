import { IEntitySpriteFactory } from "core/EntitySprite/IEntitySpriteFactory"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import * as PIXI from "pixi.js"
import {Random} from "random-js"
import { IEntitySprite } from "../EntitySprite/IEntitySprite"
import { IEntity } from "./IEntity"

export class Entity implements IEntity {
   public position: WorldPosition
   public isPlayer: boolean
   public sprites: IEntitySprite[] = []
   public speed: number = 0

   get id() {
      return this._id
   }
   protected _id: string = null
   protected _entitySpriteFactory: IEntitySpriteFactory = null
   private _random: Random

   constructor(position: WorldPosition, entitySpriteFactory: IEntitySpriteFactory, random: Random) {
      this.position = position
      this._entitySpriteFactory = entitySpriteFactory
      this._random = random
      this._id = this._random.string(32)
   }

   public onResourcesLoad(resources) {
      return
   }

}
