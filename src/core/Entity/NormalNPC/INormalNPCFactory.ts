import { IDialog } from "core/Dialog/IDialog"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { NormalNPC } from "./NormalNPC"

export interface INormalNPCFactory {
    create: (name: string, position: WorldPosition, dialogs: IDialog[]) => NormalNPC
}
