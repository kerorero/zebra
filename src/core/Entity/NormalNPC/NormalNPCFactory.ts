import { IDialog } from "core/Dialog/IDialog"
import { IDialogManager } from "core/DialogManager/IDialogManager"
import { IEntitySpriteFactory } from "core/EntitySprite/IEntitySpriteFactory"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { Random } from "random-js"
import { INormalNPCFactory } from "./INormalNPCFactory"
import { NormalNPC } from "./NormalNPC"

export class NormalNPCFactory implements INormalNPCFactory {
    set stage(value: PIXI.Container) {
        if (!this._stage) {
            this._stage = value
        }
    }
    private _stage: PIXI.Container = null
    private _entitySpriteFactory: IEntitySpriteFactory = null
    private _random: Random = null

    set dialogManager(value: IDialogManager) {
        if (!this._dialogManager) {
            this._dialogManager = value
        }
    }
    private _dialogManager: IDialogManager = null
    public constructor(
        entitySpriteFactory: IEntitySpriteFactory,
        stage: PIXI.Container,
        dialogManager: IDialogManager,
        random: Random,
    ) {
        this._entitySpriteFactory = entitySpriteFactory
        this._stage = stage
        this._dialogManager = dialogManager
        this._random = random

    }

    public create(name: string, position: WorldPosition, dialogs: IDialog[]) {
        return new NormalNPC(
            name, position, dialogs, this._entitySpriteFactory, this._stage, this._dialogManager, this._random,
        )
    }
}
