import { IAppSetting } from "core/AppSetting/IAppSetting"
import { ICamera } from "core/Camera/ICamera"
import { IDialog } from "core/Dialog/IDialog"
import { IDialogManager } from "core/DialogManager/IDialogManager"
import { EntitySprite } from "core/EntitySprite/EntitySprite"
import { EntitySpriteDirection } from "core/EntitySprite/EntitySpriteDirection"
import { IEntitySprite } from "core/EntitySprite/IEntitySprite"
import { IEntitySpriteFactory } from "core/EntitySprite/IEntitySpriteFactory"
import { IPixiRectangleFactory } from "core/PixiFactories/IPixiRectangleFactory"
import { IScene } from "core/Scene/IScene"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import * as PIXI from "pixi.js"
import { Random } from "random-js"
import { Entity } from "../Entity"
import { IEntity } from "../IEntity"

export class NormalNPC extends Entity {

   public position: WorldPosition
   public isPlayer: boolean
   public sprites: IEntitySprite[] = []
   public speed: number = 1.8

   private _defaultAnimationSpeed: number = 0.2
   private _name: string
   private _dialogs: IDialog[] = []
   private _stage: PIXI.Container = null
   private _actualSprite: PIXI.AnimatedSprite = null
   private _dialogManager: IDialogManager = null
   constructor(
      name: string,
      position: WorldPosition,
      dialogs: IDialog[],
      entitySpriteFactory: IEntitySpriteFactory,
      stage: PIXI.Container,
      dialogManager: IDialogManager,
      random: Random,
   ) {
      super(position, entitySpriteFactory, random)
      this._name = name
      this._dialogs = dialogs
      this._stage = stage
      this._dialogManager = dialogManager
      this._stage.on("onResourcesLoad", this.onResourcesLoad, this)
      this._stage.on("onPlayerStartInteraction", this.onPlayerStartInteraction, this)
   }

   public onResourcesLoad(resources) {
      const spriteDown: PIXI.AnimatedSprite = resources[`animation${this._name}WalkDown`]
      const spriteUp: PIXI.AnimatedSprite = resources[`animation${this._name}WalkUp`]
      const spriteLeft: PIXI.AnimatedSprite = resources[`animation${this._name}WalkLeft`]
      const spriteRight: PIXI.AnimatedSprite = resources[`animation${this._name}WalkRight`]

      this.sprites.push(this._entitySpriteFactory.create(spriteUp, "up"))
      this.sprites.push(this._entitySpriteFactory.create(spriteDown, "down"))
      this.sprites.push(this._entitySpriteFactory.create(spriteRight, "right"))
      this.sprites.push(this._entitySpriteFactory.create(spriteLeft, "left"))
      this._actualSprite = spriteUp
      this._actualSprite.animationSpeed = this._defaultAnimationSpeed
      this._actualSprite.position = this.position

      this._stage.addChild(this._actualSprite)
      this._stage.emit("onEntityLoaded", this)
   }

   public onPlayerStartInteraction(entity: IEntity) {
      // tslint:disable-next-line: no-console
      console.log(this._dialogs)
      if (this._dialogs.length > 0 && this.id === entity.id) {
         this._dialogManager.title = this._name
         this._dialogManager.displayDialogs(this._dialogs)
      }
   }
}
