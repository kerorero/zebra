import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { IEntitySprite } from "../EntitySprite/IEntitySprite"
export interface IEntity {
   id: string,
   position: WorldPosition,
   isPlayer: boolean,
   sprites: IEntitySprite[],
   speed: number,

   onResourcesLoad: (resources) => void
}
