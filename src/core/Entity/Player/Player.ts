import { IAppSetting } from "core/AppSetting/IAppSetting"
import { ICamera } from "core/Camera/ICamera"
import { IDialogManager } from "core/DialogManager/IDialogManager"
import { SelectDialogueOptionDirection } from "core/DialogManager/SelectDialogueOptionDirection"
import { EntitySprite } from "core/EntitySprite/EntitySprite"
import { EntitySpriteDirection } from "core/EntitySprite/EntitySpriteDirection"
import { IEntitySprite } from "core/EntitySprite/IEntitySprite"
import { IEntitySpriteFactory } from "core/EntitySprite/IEntitySpriteFactory"
import { IPixiRectangleFactory } from "core/PixiFactories/IPixiRectangleFactory"
import { IScene } from "core/Scene/IScene"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import * as PIXI from "pixi.js"
import { Random } from "random-js"
import { Entity } from "../Entity"
import { IEntity } from "../IEntity"
import { PlayerState } from "./PlayerState"

export class Player extends Entity {

   public position: WorldPosition
   public isPlayer: boolean
   public sprites: IEntitySprite[] = []
   public speed: number = 1.8

   private _stage: PIXI.Container = null
   private _actualSprite: PIXI.AnimatedSprite = null
   private _lastMoveDirection: EntitySpriteDirection = null
   private _defaultAnimationSpeed: number = 0.2
   private _scene: IScene = null
   private _appSetting: IAppSetting = null
   private _camera: ICamera = null
   private _state: PlayerState = null
   private _pixiRectangleFactory: IPixiRectangleFactory = null

   constructor(
      position: WorldPosition,
      entitySpriteFactory: IEntitySpriteFactory,
      stage: PIXI.Container,
      scene: IScene,
      appSetting: IAppSetting,
      camera: ICamera,
      pixiRectangleFactory: IPixiRectangleFactory,
      random: Random,
   ) {
      super(position, entitySpriteFactory, random)
      this._stage = stage
      this._scene = scene
      this._appSetting = appSetting
      this._camera = camera
      this._pixiRectangleFactory = pixiRectangleFactory
      this._state = PlayerState.NOTHING
      this._stage.on("onPlayerMove", this.onPlayerMove, this)
      this._stage.on("onPlayerMoveEnd", this.onPlayerMoveEnd, this)
      this._stage.on("onPlayerTrySelectDialogueQuestionOption", this.onPlayerTrySelectDialogueQuestionOption, this)
      this._stage.on("onPlayerTryStartInteraction", this.onPlayerTryStartInteraction, this)
      this._stage.on("onResourcesLoad", this.onResourcesLoad, this)
      this._stage.on("onPlayerTrySelectActiveDialogueOption", this.onPlayerTrySelectActiveDialogueOption, this)
      this._stage.on("onPlayerEndDialogueResponses", this.onPlayerEndDialogueResponses, this)
      this._stage.on("onPlayerEndDialogue", this.onPlayerEndDialogue, this)
   }

   public onResourcesLoad(resources) {
      const spriteDown: PIXI.AnimatedSprite = resources.animationPlayerWalkDown
      const spriteUp: PIXI.AnimatedSprite = resources.animationPlayerWalkUp
      const spriteLeft: PIXI.AnimatedSprite = resources.animationPlayerWalkLeft
      const spriteRight: PIXI.AnimatedSprite = resources.animationPlayerWalkRight

      this.sprites.push(this._entitySpriteFactory.create(spriteUp, "up"))
      this.sprites.push(this._entitySpriteFactory.create(spriteDown, "down"))
      this.sprites.push(this._entitySpriteFactory.create(spriteRight, "right"))
      this.sprites.push(this._entitySpriteFactory.create(spriteLeft, "left"))
      this._actualSprite = spriteUp
      this._actualSprite.position = this.position
      this._actualSprite.animationSpeed = this._defaultAnimationSpeed

      if (this._scene.config.spawn) {
         this.position = this._scene.config.spawn
         this._actualSprite.position = this.position
      }
      this._stage.addChild(this._actualSprite)
   }

   public onPlayerTrySelectDialogueQuestionOption(direction: SelectDialogueOptionDirection) {
      if (this._state === PlayerState.INTERACTION) {
         this._stage.emit("onPlayerSelectDialogueQuestionOption", direction)
      }
   }

   public onPlayerEndDialogueResponses() {
      if (this._state === PlayerState.DIALOG) {
         this._state = PlayerState.INTERACTION
      }
   }

   public onPlayerEndDialogue() {
      this._state = PlayerState.NOTHING
   }

   public onPlayerTrySelectActiveDialogueOption() {
      if (this._state === PlayerState.INTERACTION) {
         this._stage.emit("onPlayerSelectActiveDialogueOption")
         this._state = PlayerState.DIALOG
      } else if (this._state === PlayerState.DIALOG) {
         this._stage.emit("onPlayerSelectNextDialogueResponse")
      }
   }

   public onPlayerTryStartInteraction() {
      // tslint:disable-next-line: no-console
      console.log("EE")
      if (this._state === PlayerState.NOTHING) {
         const boxSize: number = 24
         const rectangle: PIXI.Rectangle = this._pixiRectangleFactory.create(
            this.position.x, this.position.y, boxSize, boxSize,
         )
         for (let x = -boxSize; x < boxSize * 2; x += boxSize) {
            for (let y = -boxSize; y < boxSize * 2; y += boxSize) {
               rectangle.enlarge(
                  this._pixiRectangleFactory.create(
                     this.position.x + x, this.position.y + y, boxSize, boxSize,
                  ),
               )
            }
         }
         // tslint:disable-next-line: no-console
         console.log("EEE")
         // tslint:disable-next-line: no-console
         console.log(this._scene.entities)
         this._scene.entities.forEach((entity: IEntity) => {
            if (rectangle.contains(entity.position.x, entity.position.y)) {
               // tslint:disable-next-line: no-console
               console.log("EEEE")
               this._stage.emit("onPlayerStartInteraction", entity)
               this._state = PlayerState.INTERACTION
            }
         })
      }
   }

   public onPlayerMove(delta: number, direction: EntitySpriteDirection) {
      if (this._state !== PlayerState.NOTHING) {
         return
      }
      if (this._lastMoveDirection !== direction) {
         this._actualSprite.stop()
         this._stage.removeChild(this._actualSprite)
         this._actualSprite = this.sprites[direction].animatedSprite
         this._actualSprite.animationSpeed = this._defaultAnimationSpeed
         this._stage.addChild(this._actualSprite)
         this._actualSprite.play()
      }
      this._lastMoveDirection = direction
      let newPlayerPos: WorldPosition = JSON.parse(JSON.stringify(this.position))
      switch (direction) {
         case EntitySpriteDirection.UP: {
            newPlayerPos.y -= this.speed * delta
            break
         }
         case EntitySpriteDirection.DOWN: {
            newPlayerPos.y += this.speed * delta
            break
         }
         case EntitySpriteDirection.RIGHT: {
            newPlayerPos.x += this.speed * delta
            break
         }
         case EntitySpriteDirection.LEFT: {
            newPlayerPos.x -= this.speed * delta
            break
         }
      }
      this._scene.collisionMap.forEach((coll: PIXI.Rectangle) => {
         if (coll.contains(newPlayerPos.x, newPlayerPos.y)) {
            newPlayerPos = JSON.parse(JSON.stringify(this.position))
            return
         }
      })

      this._actualSprite.x = newPlayerPos.x
      this._actualSprite.y = newPlayerPos.y
      this.position = newPlayerPos
      this._camera.moveCameraToContainPosition(newPlayerPos)
   }

   public onPlayerMoveEnd() {
      this._actualSprite.gotoAndStop(0)
      this._lastMoveDirection = null
   }
}
