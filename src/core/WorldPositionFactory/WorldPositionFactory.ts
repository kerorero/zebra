import { IPixiPointFactory } from "core/PixiFactories/IPixiPointFactory"
import { WorldPosition } from "core/WorldPosition/WorldPosition"

export class WorldPositionFactory implements IPixiPointFactory {
   public create(x: number, y: number) {
      return new WorldPosition(x, y)
   }
}
