import { IPixiPointFactory } from "core/PixiFactories/IPixiPointFactory"
import { ScreenPosition } from "core/WorldPosition/ScreenPosition"

export class ScreenPositionFactory implements IPixiPointFactory {
   public create(x: number, y: number) {
      return new ScreenPosition(x, y)
   }
}
