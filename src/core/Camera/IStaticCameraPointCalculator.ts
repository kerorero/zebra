import { ScreenPosition } from "core/WorldPosition/ScreenPosition"
import { StaticCameraPoint } from "./StaticCameraPoint"

export interface IStaticCameraPointCalculator {
    calculateObjectScreenPosition: (object: PIXI.DisplayObject, displayPosition: StaticCameraPoint) => ScreenPosition
}
