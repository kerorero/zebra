import { IAppSetting } from "core/AppSetting/IAppSetting"
import { ScreenPositionFactory } from "core/WorldPositionFactory/ScreenPositionFactory"
import { IStaticCameraPointCalculator } from "./IStaticCameraPointCalculator"
import { StaticCameraPoint } from "./StaticCameraPoint"

export class StaticCameraPointCalculator implements IStaticCameraPointCalculator {
    private _screenPositionFactory: ScreenPositionFactory = null
    private _appSetting: IAppSetting = null
    constructor(screenPositionFactory: ScreenPositionFactory, appSetting: IAppSetting) {
        this._screenPositionFactory = screenPositionFactory
        this._appSetting = appSetting

    }
    public calculateObjectScreenPosition(object: PIXI.DisplayObject, staticPoint: StaticCameraPoint) {
        switch (staticPoint) {
            case StaticCameraPoint.DOWN_CENTER: {
                const bounds: PIXI.Rectangle = object.getLocalBounds()
                object.pivot.x = bounds.width / 2
                object.pivot.y = bounds.height
                return this._screenPositionFactory.create(
                    this._appSetting.worldWidth / 2, this._appSetting.worldHeight,
                )
            }
        }
    }
}
