import { ScreenPosition } from "core/WorldPosition/ScreenPosition"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { StaticCameraPoint } from "./StaticCameraPoint"

export interface ICamera {
    position: WorldPosition,
    isVisibleOnCamera: (position: WorldPosition) => boolean,
    moveCameraToContainPosition: (position: WorldPosition) => void
    toLocalPosition: (position: ScreenPosition) => WorldPosition,
    attachObjectToCamera: (object: PIXI.DisplayObject, screenPosition?: ScreenPosition) => void,
    updateAttachedObjects: () => void
}
