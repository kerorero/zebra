import { IAppSetting } from "core/AppSetting/IAppSetting"
import { IPixiPointFactory } from "core/PixiFactories/IPixiPointFactory"
import { IPixiRectangleFactory } from "core/PixiFactories/IPixiRectangleFactory"
import { ScreenPosition } from "core/WorldPosition/ScreenPosition"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { ScreenPositionFactory } from "core/WorldPositionFactory/ScreenPositionFactory"
import { WorldPositionFactory } from "core/WorldPositionFactory/WorldPositionFactory"
import { ICamera } from "./ICamera"
import { StaticCameraPoint } from "./StaticCameraPoint"

export class Camera implements ICamera {
    private _visibleArea: PIXI.Rectangle = null
    private _pixiRectangleFactory: IPixiRectangleFactory = null
    private _appSetting: IAppSetting = null
    private _stage: PIXI.Container = null
    private _worldPositionFactory: WorldPositionFactory = null
    private _objectsToRecalculate: {[index: string]: PIXI.DisplayObject[]} = {}
    get position() {
        return this._worldPositionFactory.create(this._stage.pivot.x, this._stage.pivot.y)
    }

    constructor(
        pixiRectangleFactory: IPixiRectangleFactory,
        appSetting: IAppSetting,
        stage: PIXI.Container,
        worldPositionFactory: WorldPositionFactory,
    ) {
        this._pixiRectangleFactory = pixiRectangleFactory
        this._appSetting = appSetting
        this._stage = stage
        this._worldPositionFactory = worldPositionFactory
        this._visibleArea = this._pixiRectangleFactory.create(
            this._stage.pivot.x, this._stage.pivot.y,
            this._appSetting.worldWidth, this._appSetting.worldHeight,
        )
    }

    public isVisibleOnCamera(position: WorldPosition) {
        return this._visibleArea.contains(position.x, position.y)
    }

    public toLocalPosition(position: ScreenPosition) {
        return this._stage.toLocal(position) as WorldPosition
    }

    public attachObjectToCamera(object: PIXI.DisplayObject, toPosition: ScreenPosition) {
        const key = `${toPosition.x},${toPosition.y}`
        if (!this._objectsToRecalculate[key]) {
            this._objectsToRecalculate[key] = []
        }
        this._objectsToRecalculate[key].push(object)
        object.position = toPosition
    }

    public updateAttachedObjects() {
        Object.keys(this._objectsToRecalculate).forEach((positionKey: string) => {
            const objects: PIXI.DisplayObject[] = this._objectsToRecalculate[positionKey]
            objects.forEach((object: PIXI.DisplayObject) => {
                const x: number = parseFloat(positionKey.split(",")[0])
                const y: number = parseFloat(positionKey.split(",")[1])
                object.position = this.toLocalPosition(
                    this._worldPositionFactory.create(
                        x, y,
                    ),
                )
            })
        })
    }
    public moveCameraToContainPosition(position: WorldPosition) {
        if (!this.isVisibleOnCamera(position)) {
            if (!this._visibleArea.contains(position.x, this._visibleArea.y)) {
                if (position.x >= this._visibleArea.x) {
                    this._stage.pivot.x += this._visibleArea.width
                } else {
                    this._stage.pivot.x -= this._visibleArea.width
                }
            } else if (!this._visibleArea.contains(this._visibleArea.x, position.y)) {
                if (position.y >= this._visibleArea.y) {
                    this._stage.pivot.y += this._visibleArea.height
                } else {
                    this._stage.pivot.y -= this._visibleArea.height
                }
            }
            this._visibleArea = this._pixiRectangleFactory.create(
                this._stage.pivot.x, this._stage.pivot.y,
                this._appSetting.worldWidth, this._appSetting.worldHeight,
            )
            this.updateAttachedObjects()
        }
    }
}
