import { IAppSetting } from "core/AppSetting/IAppSetting"
import { IEntity } from "core/Entity/IEntity"
import { IPixiRectangleFactory } from "core/PixiFactories/IPixiRectangleFactory"
import { IPixiSpriteFactory } from "core/PixiFactories/IPixiSpriteFactory"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { WorldPositionFactory } from "core/WorldPositionFactory/WorldPositionFactory"
import $ from "jquery"
import { IConfigScene } from "./IConfigScene"
import { IScene } from "./IScene"
import { ISceneObject } from "./ISceneObject"

export class Scene implements IScene {
   protected _stage: PIXI.Container = null
   protected _entities: IEntity[] = []
   get entities(): IEntity[] {
      return this._entities
   }

   protected _pixiSpriteFactory: IPixiSpriteFactory = null
   protected _sceneName: string = ""
   get sceneName(): string {
      return this._sceneName
   }

   protected _config: IConfigScene = null
   get config(): IConfigScene {
      return this._config
   }

   protected _collisionMap: PIXI.Rectangle[] = []
   get collisionMap(): PIXI.Rectangle[] {
      return this._collisionMap
   }

   private _pixiRectangleFactory: IPixiRectangleFactory = null
   private _appSetting: IAppSetting = null
   private _worldPositionFactory: WorldPositionFactory = null
   constructor(
      stage: PIXI.Container,
      pixiSpriteFactory: IPixiSpriteFactory,
      pixiRectangleFactory: IPixiRectangleFactory,
      appSetting: IAppSetting,
      worldPositionFactory: WorldPositionFactory,
   ) {
      this._stage = stage
      this._pixiSpriteFactory = pixiSpriteFactory
      this._pixiRectangleFactory = pixiRectangleFactory
      this._worldPositionFactory = worldPositionFactory
      this._appSetting = appSetting
      this._stage.on("onResourcesLoad", this.onResourcesLoad, this)
      this._stage.on("onEntityLoaded", this.onEntityLoaded, this)
   }

   public onResourcesLoad(resources) {
      const configScene: IConfigScene = $.extend({}, resources[`scene${this._sceneName}`].data)
      configScene.objects.forEach((sceneObject: ISceneObject) => {
         const texture: PIXI.Texture = resources[`objectId${sceneObject.objectId}`].texture
         const scale = resources[`objectId${sceneObject.objectId}`].scale
         const sprite: PIXI.Sprite = this._pixiSpriteFactory.create(texture)
         this._stage.addChild(sprite)
         sprite.x = sceneObject.position.x
         sprite.y = sceneObject.position.y
         sprite.zIndex = sceneObject.z
         sprite.scale.set(scale, scale)
      })
      this._config = configScene
      this.refreshCollisionMap()
   }

   public addCollision(worldPosition: WorldPosition, width: number, height: number) {
      this._collisionMap.push(
         this._pixiRectangleFactory.create(
            worldPosition.x, worldPosition.y, width, height,
         ),
      )
   }

   public removeCollision(worldPosition: WorldPosition) {
      this._collisionMap = this._collisionMap.filter((coll) => coll.x !== worldPosition.x || coll.y !== worldPosition.y)
   }

   public refreshCollisionMap() {
      this._collisionMap = []
      this._config.collisions.forEach((coll) => {
         this.addCollision(coll, this._appSetting.mapSize, this._appSetting.mapSize)
      })
      this._entities.forEach((entity: IEntity) => {
         this.addCollision(
            this._worldPositionFactory.create(entity.position.x - 16, entity.position.y - 26), 32, 42,
         )
      })
   }

   public onEntityLoaded(entity: IEntity) {
      this._entities.push(entity)
      this.refreshCollisionMap()
   }

}
