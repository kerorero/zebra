import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { IConfigScene } from "./IConfigScene"
import { ISceneObject } from "./ISceneObject"

export class ConfigScene implements IConfigScene {
   public objects: ISceneObject[]
   public collisions: WorldPosition[]
   public spawn: WorldPosition
   constructor(objects: ISceneObject[], collisions: WorldPosition[] = [], spawn: WorldPosition = null) {
      this.objects = objects
      this.collisions = collisions
      this.spawn = spawn
   }
}
