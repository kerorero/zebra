import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { IConfigScene } from "./IConfigScene"
import { ISceneObject } from "./ISceneObject"

export interface IConfigSceneFactory {
   create: (objects: ISceneObject[], collsions?: WorldPosition[]) => IConfigScene
}
