import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { ConfigScene } from "./ConfigScene"
import { IConfigScene } from "./IConfigScene"
import { IConfigSceneFactory } from "./IConfigSceneFactory"
import { ISceneObject } from "./ISceneObject"

export class ConfigSceneFactory implements IConfigSceneFactory {
   public create(objects: ISceneObject[], collisions: WorldPosition[] = [], spawn: WorldPosition = null) {
      return new ConfigScene(objects, collisions, spawn)
   }
}
