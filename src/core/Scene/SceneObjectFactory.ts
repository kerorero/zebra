import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { ISceneObjectFactory } from "./ISceneObjectFactory"
import { SceneObject } from "./SceneObject"

export class SceneObjectFactory implements ISceneObjectFactory {
   public create(worldPosition: WorldPosition, objectId: number, zIndex: number) {
      return new SceneObject(worldPosition, objectId, zIndex)
   }
}
