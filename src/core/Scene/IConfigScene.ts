import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { ISceneObject } from "./ISceneObject"

export interface IConfigScene {
   objects: ISceneObject[]
   collisions: WorldPosition[],
   spawn: WorldPosition
}
