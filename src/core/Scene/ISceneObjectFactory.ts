import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { IConfigScene } from "./IConfigScene"
import { ISceneObject } from "./ISceneObject"

export interface ISceneObjectFactory {
   create: (worldPosition: WorldPosition, objectId: number, zIndex: number) => ISceneObject
}
