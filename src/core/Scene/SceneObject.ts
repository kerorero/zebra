import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { ISceneObject } from "./ISceneObject"

export class SceneObject implements ISceneObject {
   public position: WorldPosition
   public objectId: number
   public z: number

   constructor(position: WorldPosition, objectId: number, z: number) {
      this.position = position,
      this.objectId = objectId
      this.z = z
   }
}
