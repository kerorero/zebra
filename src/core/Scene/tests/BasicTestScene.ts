import { IAppSetting } from "core/AppSetting/IAppSetting"
import { IPixiRectangleFactory } from "core/PixiFactories/IPixiRectangleFactory"
import { IPixiSpriteFactory } from "core/PixiFactories/IPixiSpriteFactory"
import { WorldPositionFactory } from "core/WorldPositionFactory/WorldPositionFactory"
import { Scene } from "../Scene"

export class BasicTestScene extends Scene {
   constructor(
      stage: PIXI.Container,
      pixiSpriteFactory: IPixiSpriteFactory,
      pixiRectangleFactory: IPixiRectangleFactory,
      appSetting: IAppSetting,
      worldPositionFactory: WorldPositionFactory,
   ) {
      super(stage, pixiSpriteFactory, pixiRectangleFactory, appSetting, worldPositionFactory)
      this._sceneName = "BasicTest"
   }

   public onResourcesLoad(resources) {
      super.onResourcesLoad(resources)
   }

}
