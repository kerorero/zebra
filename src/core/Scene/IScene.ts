import { IEntity } from "core/Entity/IEntity"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { IConfigScene } from "./IConfigScene"

export interface IScene {
   onResourcesLoad: (resources) => void,
   refreshCollisionMap: () => void,
   addCollision: (worldPosition: WorldPosition, width: number, height: number) => void,
   removeCollision: (worldPosition: WorldPosition) => void,
   sceneName: string
   config: IConfigScene,
   collisionMap: PIXI.Rectangle[]
   entities: IEntity[]
}
