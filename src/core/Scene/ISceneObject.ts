import { WorldPosition } from "core/WorldPosition/WorldPosition"

export interface ISceneObject {
   position: WorldPosition
   objectId: number,
   z: number
}
