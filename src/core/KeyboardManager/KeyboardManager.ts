import { AppSetting } from "core/AppSetting/AppSetting"
import { SelectDialogueOptionDirection } from "core/DialogManager/SelectDialogueOptionDirection"
import { EntitySpriteDirection } from "core/EntitySprite/EntitySpriteDirection"
import { SceneEditorChangeObjectDirection } from "core/SceneEditor/SceneEditorChangeObjectDirection"
import { SceneEditorChangeZDirection } from "core/SceneEditor/SceneEditorChangeZDirection"
import { SceneEditorModes } from "core/SceneEditor/SceneEditorModes"

export class KeyboardManager {
   private _pressedKey: string = null
   private _stage: PIXI.Container = null
   private _mappedCodesForMove = {}
   private _mappedCodesForSceneEditor = {}
   private _mappedCodesForSceneEditorZIndexControl = {}
   private _mappedCodesForSaveSceneEditor = {}
   private _mappedCodesForSelectDialogueOption = {}
   private _appSetting: AppSetting = null
   private _isWindowOpened: {[key: string]: boolean} = {}

   constructor(stage: PIXI.Container, appSetting: AppSetting) {
      this._mappedCodesForMove = {
         KeyA: EntitySpriteDirection.LEFT,
         KeyD: EntitySpriteDirection.RIGHT,
         KeyS: EntitySpriteDirection.DOWN,
         KeyW: EntitySpriteDirection.UP,
      }
      this._mappedCodesForSceneEditor = {
         ArrowLeft: SceneEditorChangeObjectDirection.DOWN,
         ArrowRight: SceneEditorChangeObjectDirection.UP,
      }
      this._mappedCodesForSceneEditorZIndexControl = {
         ArrowDown: SceneEditorChangeZDirection.DOWN,
         ArrowUp: SceneEditorChangeZDirection.UP,
      }
      this._mappedCodesForSelectDialogueOption = {
         KeyS: SelectDialogueOptionDirection.DOWN,
         KeyW: SelectDialogueOptionDirection.UP,
      }
      this._appSetting = appSetting
      this._stage = stage
      this._stage.on("onKeyDown", this.onKeyDown, this)
      this._stage.on("onUpdate", this.onUpdate, this)
      this._stage.on("onKeyUp", this.onKeyUp, this)
   }

   public onUpdate(delta: number) {
      if (this._pressedKey && this._mappedCodesForMove) {
         if (this._isKeyForPlayerMove(this._pressedKey)) {
            const direction: EntitySpriteDirection = this._mappedCodesForMove[this._pressedKey]
            this._stage.emit("onPlayerMove", delta, direction)
            this._stage.emit("onSceneEditorMoveCamera", delta, direction)
         }
      }
   }

   public onKeyDown(event: KeyboardEvent) {
      if (this._isKeyForPlayerMove(this._pressedKey) && !this._isKeyForPlayerMove(event.code)) {
         this._stage.emit("onPlayerMoveEnd")
      }
      if (this._isKeyForInteraction(event.code)) {
         this._stage.emit("onPlayerTryStartInteraction")
      }
      if (this._isKeyForSelectDialogueQuestionOption(event.code)) {
         this._stage.emit(
            "onPlayerTrySelectDialogueQuestionOption",
            this._mappedCodesForSelectDialogueOption[event.code],
         )
      }
      if (this._isKeyForSelectActiveDialogueOption(event.code)) {
         this._stage.emit("onPlayerTrySelectActiveDialogueOption")
      }
      if (this._appSetting.sceneBuilder) {
         if (this._isKeyForChangeObjectInEditor(event.code)) {
            this._stage.emit("onSceneEditorChangeObject", this._mappedCodesForSceneEditor[event.code])
         } else if (this._isKeyForSaveMapInEditor(event.code)) {
            this._stage.emit("onSceneEditorSaveMap")
         } else if (this._isKeyForResetCameraInEditor(event.code)) {
            this._stage.emit("onSceneEditorResetCamera")
         } else if (this._isKeyForRemoveObjectInEditor(event.code)) {
            this._stage.emit("onSceneEditorRemoveObject")
         } else if (this._isKeyForChooseObjectWindowInEditor(event.code)) {
            if (this._isWindowOpened.SceneEditorChooseObject) {
               this._stage.emit("onWindowClose", "SceneEditorChooseObject")
            } else {
               this._stage.emit("onWindowOpen", "SceneEditorChooseObject")
            }
            this._isWindowOpened.SceneEditorChooseObject = !this._isWindowOpened.SceneEditorChooseObject
         } else if (this._isKeyForChangeModeInEditor(event.code)) {
            this._stage.emit("onSceneEditorChangeMode")
         } else if (this._isKeyForChangeLayerIndexInEditor(event.code)) {
            this._stage.emit("onSceneEditorChangeLayer", this._mappedCodesForSceneEditorZIndexControl[event.code])
         }
      }

      this._pressedKey = event.code
   }

   public onKeyUp(event: KeyboardEvent) {
      if (event.code === this._pressedKey) {
         this._pressedKey = null
         if (this._isKeyForPlayerMove(event.code)) {
            this._stage.emit("onPlayerMoveEnd")
         }
      }
   }

   private _isKeyForPlayerMove(keyCode) {
      return (Object.keys(this._mappedCodesForMove).includes(keyCode))
   }

   private _isKeyForSelectDialogueQuestionOption(keyCode) {
      return  (Object.keys(this._mappedCodesForSelectDialogueOption).includes(keyCode))
   }

   private _isKeyForChangeObjectInEditor(keyCode) {
      return  (Object.keys(this._mappedCodesForSceneEditor).includes(keyCode))
   }

   private _isKeyForResetCameraInEditor(keyCode) {
      return keyCode === "KeyR"
   }

   private _isKeyForSaveMapInEditor(keyCode) {
      return keyCode === "KeyY"
   }

   private _isKeyForRemoveObjectInEditor(keyCode) {
      return keyCode === "KeyX"
   }

   private _isKeyForChooseObjectWindowInEditor(keyCode) {
      return keyCode === "KeyT"
   }

   private _isKeyForChangeModeInEditor(keyCode) {
      return keyCode === "KeyM"
   }

   private _isKeyForInteraction(keyCode) {
      return keyCode === "KeyE"
   }

   private _isKeyForSelectActiveDialogueOption(keyCode) {
      return keyCode === "Space"
   }

   private _isKeyForChangeLayerIndexInEditor(keyCode) {
      return (Object.keys(this._mappedCodesForSceneEditorZIndexControl).includes(keyCode))
   }
}
