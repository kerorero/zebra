import { DialogResponseFunctionName } from "./DialogResponseFunctionName"

export interface IDialogResponseFunction {
    name: DialogResponseFunctionName,
    extraArgs: string[] | null
}
