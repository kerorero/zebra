import { IDialog } from "./IDialog"
import { IDialogQuestion } from "./IDialogQuestion"
import { IDialogResponseFunction } from "./IDialogResponseFunction"

export interface IDialogResponse {
    response: string,
    dialog: IDialog | null,
    functions: IDialogResponseFunction[] | null
}
