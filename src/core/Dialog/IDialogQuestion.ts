import { IDialogResponse } from "./IDialogResponse"

export interface IDialogQuestion {
    question: string,
    responses: IDialogResponse[]
}
