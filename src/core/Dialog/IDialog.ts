import { IDialogQuestion } from "./IDialogQuestion"

export interface IDialog {
    questions: IDialogQuestion[],
    uniqueName: string | null
}
