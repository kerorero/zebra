import { IAppSetting } from "./IAppSetting"

export class AppSetting implements IAppSetting {
   public worldWidth: number
   public worldHeight: number
   public mapSize: number
   public mainDiv: string
   public mapBackgroundColor: number
   public sceneBuilder: boolean
}
