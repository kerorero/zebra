export interface IAppSetting {
   worldWidth: number,
   worldHeight: number,
   mapSize: number,
   mainDiv: string,
   mapBackgroundColor: number,
   sceneBuilder: boolean,
}
