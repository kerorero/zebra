import { IPixiRectangleFactory } from "core/PixiFactories/IPixiRectangleFactory"
import { IPixiSpriteFactory } from "core/PixiFactories/IPixiSpriteFactory"
import { IPixiTextureFactory } from "core/PixiFactories/IPixiTextureFactory"
import * as PIXI from "pixi.js"
import { ISpritesheetManager } from "./ISpritesheetManager"

export class SpritesheetManager implements ISpritesheetManager {
   private _pixiTextureFactory: IPixiTextureFactory = null
   private _pixiRectangleFactory: IPixiRectangleFactory = null
   private _spritesheetId: string = ""
   get spritesheetId(): string {
      return this._spritesheetId
   }

   set spritesheetId(value: string) {
      this._spritesheetId = value
   }

   private _objectSize: number = 0
   get objectSize(): number {
      return this._objectSize
   }

   set objectSize(value: number) {
      this._objectSize = value
   }

   private _extraSpace: number = 0
   get extraSpace(): number {
      return this._extraSpace
   }

   set extraSpace(value: number) {
      this._extraSpace = value
   }

   private _scale: number = 0
   get scale(): number {
      return this._scale
   }

   set scale(value: number) {
      this._scale = value
   }

   constructor(pixiTextureFactory: IPixiTextureFactory, pixiRectangleFactory: IPixiRectangleFactory) {
      this._pixiTextureFactory = pixiTextureFactory
      this._pixiRectangleFactory = pixiRectangleFactory
   }

   public load(resources, startFromId= 0) {
      const texture: PIXI.Texture = resources[`spritesheet${this._spritesheetId}`].texture
      const rows: number = Math.floor(texture.height / this._objectSize) * this._objectSize
      const cols: number = Math.floor(texture.width / this._objectSize) * this._objectSize
      for (let y: number = 0; y < rows; y += this._objectSize) {
         for (let x: number = 0; x < cols; x += this._objectSize) {
            const rectangle = this._pixiRectangleFactory.create(x, y, this._objectSize, this._objectSize)
            const createdTexture = this._pixiTextureFactory.create(texture.baseTexture, rectangle)
            resources[`objectId${startFromId}`] = {texture: createdTexture, scale: this._scale}
            startFromId++
            x += this._extraSpace
         }
         y += this._extraSpace
      }
      return [resources, startFromId]
   }
}
