import { IPixiRectangleFactory } from "core/PixiFactories/IPixiRectangleFactory"
import { IPixiSpriteFactory } from "core/PixiFactories/IPixiSpriteFactory"
import { IPixiTextureFactory } from "core/PixiFactories/IPixiTextureFactory"
import * as PIXI from "pixi.js"
import { ISpriteManager } from "./ISpriteManager"
import { ISpritesheetManager } from "./ISpritesheetManager"

export class SpriteManager implements ISpriteManager {
   private _pixiSpriteFactory: IPixiSpriteFactory = null

   constructor(pixiSpriteFactory: IPixiSpriteFactory) {
      this._pixiSpriteFactory = pixiSpriteFactory
   }

   public load(texture: PIXI.Texture) {
      const newSprite: PIXI.Sprite = this._pixiSpriteFactory.create(texture)
      return newSprite
   }
}
