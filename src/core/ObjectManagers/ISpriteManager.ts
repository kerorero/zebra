import * as PIXI from "pixi.js"
export interface ISpriteManager {
   load: (texture: PIXI.Texture) => PIXI.Sprite
}
