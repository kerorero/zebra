export interface IAnimatedSpriteManager {
   load: (resources, frames: number) => any
   animationName: string
}
