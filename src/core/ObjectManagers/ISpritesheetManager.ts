import * as PIXI from "pixi.js"
export interface ISpritesheetManager {
   load: (resource, startFromId: number) => any[]
   objectSize: number
   scale: number
   spritesheetId: string
   extraSpace: number
}
