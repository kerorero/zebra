import { IPixiAnimatedSpriteFactory } from "core/PixiFactories/IPixiAnimatedSpriteFactory"
import * as PIXI from "pixi.js"
import { IAnimatedSpriteManager } from "./IAnimatedSpriteManager"

export class AnimatedSpriteManager implements IAnimatedSpriteManager {
   private _pixiAnimatedSpriteFactory: IPixiAnimatedSpriteFactory = null
   private _spritesIds: string[] = null
   private _animationName: string = ""
   get animationName(): string {
      return this._animationName
   }

   set animationName(value: string) {
      this._animationName = value
   }

   constructor(pixiAnimatedSpriteFactory: IPixiAnimatedSpriteFactory) {
      this._pixiAnimatedSpriteFactory = pixiAnimatedSpriteFactory
   }

   public load(resources, frames: number) {
      const framesTextures: PIXI.Texture[] = []
      for (let i = 0; i < frames; i++) {
         framesTextures.push(resources[`frameForAnimation${this._animationName}${i}`].texture)
      }

      const animatedSprite: PIXI.AnimatedSprite = this._pixiAnimatedSpriteFactory.create(framesTextures, true)
      animatedSprite.anchor.set(0.5)
      resources[`animation${this._animationName}`] = animatedSprite
      return resources
   }
}
