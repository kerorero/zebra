import * as PIXI from "pixi.js"
export interface IEntitySprite {
   animatedSprite: PIXI.AnimatedSprite,
   direction: string,
}
