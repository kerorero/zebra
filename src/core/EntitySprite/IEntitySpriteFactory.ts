import * as PIXI from "pixi.js"
import { EntitySprite } from "./EntitySprite"
export interface IEntitySpriteFactory {
   create: (animatedSprite: PIXI.AnimatedSprite, direction: string) => EntitySprite
}
