import * as PIXI from "pixi.js"
import { EntitySprite } from "./EntitySprite"
import { IEntitySprite } from "./IEntitySprite"
import { IEntitySpriteFactory } from "./IEntitySpriteFactory"

export class EntitySpriteFactory implements IEntitySpriteFactory {
   public create(animatedSprite: PIXI.AnimatedSprite, direction: string) {
      return new EntitySprite(animatedSprite, direction)
   }
}
