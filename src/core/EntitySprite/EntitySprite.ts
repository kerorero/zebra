import * as PIXI from "pixi.js"
import { IEntitySprite } from "./IEntitySprite"

export class EntitySprite implements IEntitySprite {
   public animatedSprite: PIXI.AnimatedSprite = null
   public direction: string = null

   constructor(animatedSprite: PIXI.AnimatedSprite, direction: string) {
      this.animatedSprite = animatedSprite
      this.direction = direction
   }
}
