import { IEntity } from "core/Entity/IEntity"
import { INormalNPCFactory } from "core/Entity/NormalNPC/INormalNPCFactory"
import { INPCConfig } from "core/NPCManager/INPCConfig"
import { INPCManager } from "./INPCManager"

export class NPCManager implements INPCManager {
    private _normalNpcFactory: INormalNPCFactory
    constructor(normalNpcFactory: INormalNPCFactory) {
        this._normalNpcFactory = normalNpcFactory
    }

    public load(npcConfig: INPCConfig) {
        return  this._normalNpcFactory.create(npcConfig.name, npcConfig.spawn, npcConfig.dialogs)
    }
}
