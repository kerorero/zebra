import { IDialog } from "core/Dialog/IDialog"
import { WorldPosition } from "core/WorldPosition/WorldPosition"

export interface INPCConfig {
    name: string,
    spawn: WorldPosition,
    dialogs: IDialog[]
}
