import { AppSetting } from "core/AppSetting/AppSetting"
import { ICamera } from "core/Camera/ICamera"
import { EntitySpriteDirection } from "core/EntitySprite/EntitySpriteDirection"
import { SceneEditorChangeObjectDirection } from "core/SceneEditor/SceneEditorChangeObjectDirection"
import { ScreenPositionFactory } from "core/WorldPositionFactory/ScreenPositionFactory"
import { WorldPositionFactory } from "core/WorldPositionFactory/WorldPositionFactory"

export class MouseManager {
   private _stage: PIXI.Container = null
   private _clicked: boolean = false
   private _rightClicked: boolean = false
   private _appSetting: AppSetting = null
   private _worldPositionFactory: WorldPositionFactory = null
   private _screenPositionFactory: ScreenPositionFactory = null
   private _camera: ICamera = null

   constructor(
      stage: PIXI.Container,
      appSetting: AppSetting,
      worldPositionFactory: WorldPositionFactory,
      screenPositionFactory: ScreenPositionFactory,
      camera: ICamera,
   ) {
      this._appSetting = appSetting
      this._stage = stage
      this._worldPositionFactory = worldPositionFactory
      this._screenPositionFactory = screenPositionFactory
      this._camera = camera
      this._stage.on("onMouseRightButtonDown", this.onMouseRightButtonDown, this)
      this._stage.on("onMouseRightButtonUp", this.onMouseRightButtonUp, this)
      this._stage.on("onMouseUp", this.onMouseUp, this)
      this._stage.on("onMouseDown", this.onMouseDown, this)
      this._stage.on("onMouseMove", this.onMouseMove, this)
   }

   public onMouseDown(event: PIXI.interaction.InteractionEvent) {
      this._clicked = true
      this._stage.emit("onPlayerClick", this._camera.toLocalPosition(
         event.data.global,
      ))
      this._stage.emit("onScreenClick", this._screenPositionFactory.create(
         event.data.global.x, event.data.global.y,
      ))
   }

   public onMouseUp(event: PIXI.interaction.InteractionEvent) {
      this._clicked = false
   }

   public onMouseMove(event: PIXI.interaction.InteractionEvent) {
      if (this._clicked) {
         this._stage.emit("onPlayerDrag", this._camera.toLocalPosition(
            event.data.global,
         ))
         this._stage.emit("onScreenDrag", this._screenPositionFactory.create(
            event.data.global.x, event.data.global.y,
         ))
      }
   }

   public onMouseRightButtonDown(event: PIXI.interaction.InteractionEvent) {
      this._rightClicked = true
      this._stage.emit("onPlayerClickRightButton", this._camera.toLocalPosition(
         event.data.global,
      ))
      this._stage.emit("onScreenClickRightButton", this._screenPositionFactory.create(
         event.data.global.x, event.data.global.y,
      ))
   }

   public onMouseRightButtonUp(event: PIXI.interaction.InteractionEvent) {
      this._rightClicked = false
   }
}
