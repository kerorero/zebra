import * as PIXI from "pixi.js"
export class ScreenPosition extends PIXI.Point {
   public x: number
   public y: number

   constructor(x?: number, y?: number) {
      super(x, y)
   }

   public toString = () => {
      return this.x + "|" + this.y
   }
}
