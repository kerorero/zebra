import { AppSetting } from "core/AppSetting/AppSetting"
import { IAppSetting } from "core/AppSetting/IAppSetting"
import { ICamera } from "core/Camera/ICamera"
import { EntitySpriteDirection } from "core/EntitySprite/EntitySpriteDirection"
import { IPixiSpriteFactory } from "core/PixiFactories/IPixiSpriteFactory"
import { IPixiTextFactory } from "core/PixiFactories/IPixiTextFactory"
import { PixiSpriteFactory } from "core/PixiFactories/PixiSpriteFactory"
import { IConfigScene } from "core/Scene/IConfigScene"
import { IConfigSceneFactory } from "core/Scene/IConfigSceneFactory"
import { IScene } from "core/Scene/IScene"
import { ISceneObject } from "core/Scene/ISceneObject"
import { ISceneObjectFactory } from "core/Scene/ISceneObjectFactory"
import { WorldPosition } from "core/WorldPosition/WorldPosition"
import { WorldPositionFactory } from "core/WorldPositionFactory/WorldPositionFactory"
import { saveAs } from "file-saver"
import { DateTime } from "luxon"
import { SceneEditorChangeObjectDirection } from "./SceneEditorChangeObjectDirection"
import { SceneEditorChangeZDirection } from "./SceneEditorChangeZDirection"
import { SceneEditorModes } from "./SceneEditorModes"
export class SceneEditor {
   private _stage: PIXI.Container = null
   private _z: number = 0
   private _pixiSpriteFactory: IPixiSpriteFactory = null
   private _appSetting: IAppSetting = null
   private _resources: any[] = []
   private _actualSelectedObjectId: number = 0
   private _spriteForActualTexture: PIXI.Sprite = null
   private _configSceneFactory: IConfigSceneFactory = null
   private _sceneObjects: {[key: string]: ISceneObject } = {}
   private _sceneObjectFactory: ISceneObjectFactory = null
   private _pixiTextFactory: IPixiTextFactory = null
   private _worldPositionFactory: WorldPositionFactory = null
   private _removeMode: boolean = false
   private _scene: IScene = null
   private _isAnyWindowOpen: boolean = false
   private _actualMode: SceneEditorModes = SceneEditorModes.NORMAL
   private _collisions: WorldPosition[] = []
   private _modeText: PIXI.Text = null
   private _selectedText: PIXI.Text = null
   private _selectedIdText: PIXI.Text = null
   private _selectedZText: PIXI.Text = null
   private _camera: ICamera = null
   private _moveCameraSpeed: number = 5
   private _lastPivotPosition: WorldPosition = null

   private _allModes: SceneEditorModes[] = [
      SceneEditorModes.NORMAL,
      SceneEditorModes.COLLISION,
      SceneEditorModes.CAMERA,
   ]

   constructor(
      stage: PIXI.Container,
      pixiSpriteFactory: IPixiSpriteFactory,
      appSetting: IAppSetting,
      configSceneFactory: IConfigSceneFactory,
      sceneObjectFactory: ISceneObjectFactory,
      worldPositionFactory: WorldPositionFactory,
      scene: IScene,
      pixiTextFactory: IPixiTextFactory,
      camera: ICamera,
   ) {
      this._stage = stage
      this._pixiSpriteFactory = pixiSpriteFactory
      this._appSetting = appSetting
      this._configSceneFactory = configSceneFactory
      this._sceneObjectFactory = sceneObjectFactory
      this._worldPositionFactory = worldPositionFactory
      this._scene = scene
      this._pixiTextFactory = pixiTextFactory
      this._camera = camera
      this._isAnyWindowOpen = false
      this._stage.on("onResourcesLoad", this.onResourcesLoad, this)
      this._stage.on("onPlayerClick", this.onPlayerClick, this)
      this._stage.on("onPlayerDrag", this.onPlayerDrag, this)
      this._stage.on("onWindowOpen", this.onWindowOpen, this)
      this._stage.on("onWindowClose", this.onWindowClose, this)
      this._stage.on("onSceneEditorChangeObject", this.onSceneEditorChangeObject, this)
      this._stage.on("onSceneEditorSaveMap", this.onSceneEditorSaveMap, this)
      this._stage.on("onSceneEditorChangeMode", this.onSceneEditorChangeMode, this)
      this._stage.on("onSceneEditorChangeLayer", this.onSceneEditorChangeLayer, this)
      this._stage.on("onSceneEditorMoveCamera", this.onSceneEditorMoveCamera, this)
      this._stage.on("onSceneEditorResetCamera", this.onSceneEditorResetCamera, this)
      this._stage.on("onPlayerSelectObjectFromWindow", this.onPlayerSelectObjectFromWindow, this)
      this._stage.on("onPlayerClickRightButton", this.onPlayerClickRightButton, this) //

   }

   public onResourcesLoad(resources) {
      this._resources = resources
      const configScene: IConfigScene = this._scene.config
      configScene.objects.forEach((object) => {
         this._sceneObjects[`${object.objectId}|${object.position.x}|${object.position.y}`] = object
      })
      configScene.collisions.forEach((collision) => {
         const worldPosition = this._worldPositionFactory.create(collision.x, collision.y)
         this._collisions.push(worldPosition)
      })
      const styleObject = {fontFamily: "Arial", fontSize: 14, fill : 0x00bbff}
      this._selectedText = this._pixiTextFactory.create(`Selected:`, styleObject)
      this._selectedIdText = this._pixiTextFactory.create(
         `Selected object id: ${this._actualSelectedObjectId}`,
         styleObject,
      )
      this._modeText = this._pixiTextFactory.create(`Mode: ${this._actualMode}`, styleObject)
      this._selectedZText = this._pixiTextFactory.create(`Z: ${this._z}`, styleObject)

      const textRequiredAdd = [this._selectedText, this._selectedIdText, this._modeText, this._selectedZText]
      let posY = 0
      textRequiredAdd.forEach((text) => {
         text.x = 0
         text.y = posY
         posY += this._appSetting.mapSize
         this._stage.addChild(text)
         this._camera.attachObjectToCamera(text, text.position as PIXI.Point)
      })
      const texture: PIXI.Texture = this._resources[`objectId${this._actualSelectedObjectId}`].texture
      const scale = this._resources[`objectId${this._actualSelectedObjectId}`].scale
      this._spriteForActualTexture = this._pixiSpriteFactory.create(texture)
      this._spriteForActualTexture.x = 64
      this._spriteForActualTexture.y = 0
      this._spriteForActualTexture.scale.set(scale, scale)
      this._stage.addChild(this._spriteForActualTexture)
      this._camera.attachObjectToCamera(
         this._spriteForActualTexture,
         this._spriteForActualTexture.position as PIXI.Point,
      )
      this._createPreviewTexture()
   }

   public onSceneEditorChangeMode() {

      let actualModeIndex = this._allModes.indexOf(this._actualMode)
      if (actualModeIndex >= this._allModes.length - 1) {
         actualModeIndex = 0
      } else {
         actualModeIndex++
      }
      this._actualMode = this._allModes[actualModeIndex]
      this._modeText.text = `Mode: ${this._actualMode}`
      this._stage.children.forEach((child: PIXI.Sprite) => {
         child.tint = 0xFFFFFF
      })
      switch (this._actualMode) {
         case  SceneEditorModes.COLLISION: {
            this._collisions.forEach((collision) => {
               this._addCollision(collision)
             })
         }
      }
   }

   public onWindowOpen() {
      this._isAnyWindowOpen = true
   }

   public onWindowClose() {
      this._isAnyWindowOpen = false
   }

   public onPlayerClick(position: WorldPosition) {
      if (this._isAnyWindowOpen) {
         return
      }
      position.x = Math.floor(position.x / this._appSetting.mapSize) * this._appSetting.mapSize
      position.y = Math.floor(position.y / this._appSetting.mapSize) * this._appSetting.mapSize
      if (this._actualMode === SceneEditorModes.COLLISION) {
         this._addCollision(position)
      } else {
         this._createActualTexture(position)
      }
   }

   public onPlayerDrag(position: WorldPosition) {
      if (this._isAnyWindowOpen) {
         return
      }
      position.x = Math.floor(position.x / this._appSetting.mapSize) * this._appSetting.mapSize
      position.y = Math.floor(position.y / this._appSetting.mapSize) * this._appSetting.mapSize
      if (this._actualMode === SceneEditorModes.COLLISION) {
         this._addCollision(position)
      } else {
         this._createActualTexture(position)
      }
   }

   public onPlayerClickRightButton(position: WorldPosition) {
      if (this._isAnyWindowOpen) {
         return
      }
      position.x = Math.floor(position.x / this._appSetting.mapSize) * this._appSetting.mapSize
      position.y = Math.floor(position.y / this._appSetting.mapSize) * this._appSetting.mapSize
      if (this._actualMode === SceneEditorModes.COLLISION) {
         this._removeCollision(position)
      } else {
         this._removeObjectOnPosition(position)
      }
   }

   public onSceneEditorResetCamera() {
      if (this._actualMode === SceneEditorModes.CAMERA && this._lastPivotPosition !== null) {
         this._stage.pivot.x = this._lastPivotPosition.x
         this._stage.pivot.y = this._lastPivotPosition.y
         this._camera.updateAttachedObjects()
         this._lastPivotPosition = null
      }
   }

   public onSceneEditorMoveCamera(delta: number, direction: EntitySpriteDirection) {
      if (this._actualMode !== SceneEditorModes.CAMERA) {
         return
      }
      if (this._lastPivotPosition === null) {
         this._lastPivotPosition = this._worldPositionFactory.create(this._stage.pivot.x, this._stage.pivot.y)
      }
      const newPos: WorldPosition = this._stage.pivot as PIXI.Point
      switch (direction) {
         case EntitySpriteDirection.UP: {
            newPos.y -= this._moveCameraSpeed * delta
            break
         }
         case EntitySpriteDirection.DOWN: {
            newPos.y += this._moveCameraSpeed * delta
            break
         }
         case EntitySpriteDirection.RIGHT: {
            newPos.x += this._moveCameraSpeed * delta
            break
         }
         case EntitySpriteDirection.LEFT: {
            newPos.x -= this._moveCameraSpeed * delta
            break
         }
      }
      this._stage.pivot = newPos
      this._camera.updateAttachedObjects()
   }

   public onPlayerSelectObjectFromWindow(objectId: number) {
      this._actualSelectedObjectId = objectId
      this._createPreviewTexture()
   }

   public onSceneEditorChangeLayer(direction: SceneEditorChangeZDirection) {
      this._z += direction
      this._selectedZText.text = `Z: ${this._z}`
   }

   public onSceneEditorSaveMap() {
      const newScene = this._configSceneFactory.create(Object.values(this._sceneObjects), this._collisions)
      saveAs(new Blob([JSON.stringify(newScene)], {type: "application/json;charset=utf-8"}), "unknownMap.json")
   }

   public onSceneEditorChangeObject(direction: SceneEditorChangeObjectDirection) {
      this._actualSelectedObjectId += direction
      if (this._actualSelectedObjectId < 0 || this._actualSelectedObjectId > Object.keys(this._resources).length - 1) {
         this._actualSelectedObjectId = 0
      }
      this._createPreviewTexture()
   }

   private _addCollision(position: WorldPosition) {
      position = this._worldPositionFactory.create(position.x, position.y)
      if (this._collisions.filter((e) => e.toString() === position.toString()).length <= 0) {
         this._collisions.push(position)
         this._scene.addCollision(position, this._appSetting.mapSize, this._appSetting.mapSize)
      }
      Object.keys(this._sceneObjects).forEach((key) => {
         if (key.endsWith(position.toString())) {
            this._stage.children.forEach((child: PIXI.Sprite) => {
               if (child.x === position.x && child.y === position.y) {
                  child.tint = 0xb06a1a
               }
            })
         }
      })
   }

   private _removeCollision(position: WorldPosition) {
      position = this._worldPositionFactory.create(position.x, position.y)
      this._collisions = this._collisions.filter((coll) => coll.toString() !== position.toString())
      this._scene.removeCollision(position)
      Object.keys(this._sceneObjects).forEach((key) => {
         if (key.endsWith(`${position.x}|${position.y}`)) {
            this._stage.children.forEach((child: PIXI.Sprite) => {
               if (child.x === position.x && child.y === position.y) {
                  child.tint = 0xFFFFFF
               }
            })
         }
      })
   }

   private _createPreviewTexture() {
      this._selectedIdText.text = `Selected object id: ${this._actualSelectedObjectId}`
      const texture: PIXI.Texture = this._resources[`objectId${this._actualSelectedObjectId}`].texture
      const scale = this._resources[`objectId${this._actualSelectedObjectId}`].scale
      this._spriteForActualTexture.texture = texture
      this._spriteForActualTexture.scale.set(scale, scale)
   }

   private _removeObjectOnPosition(position: WorldPosition) {
      position = this._worldPositionFactory.create(position.x, position.y)
      const savedPosition: WorldPosition = position
      savedPosition.x = position.x
      savedPosition.y = position.y
      Object.values(this._sceneObjects).forEach((obj, index) => {
         if (obj.position.x === position.x && obj.position.y === position.y && this._z === obj.z) {
            const objectKey = Object.keys(this._sceneObjects).find((key) => this._sceneObjects[key] === obj)
            delete this._sceneObjects[objectKey]
            this._stage.children.forEach((child) => {
               if (child.x === obj.position.x && child.y === obj.position.y && this._z === obj.z) {
                  this._stage.removeChild(child)
               }
            })
         }
      })
   }

   private _createActualTexture(position: WorldPosition) {
      position = this._worldPositionFactory.create(position.x, position.y)
      const texture = this._resources[`objectId${this._actualSelectedObjectId}`].texture
      const scale = this._resources[`objectId${this._actualSelectedObjectId}`].scale
      const newSprite = this._pixiSpriteFactory.create(texture)
      newSprite.x = position.x
      newSprite.y = position.y
      newSprite.zIndex = this._z
      newSprite.scale.set(scale, scale)
      this._stage.addChild(newSprite)

      const worldPosition: WorldPosition = this._worldPositionFactory.create(newSprite.x, newSprite.y)
      const sceneObject: ISceneObject = this._sceneObjectFactory.create(
         worldPosition, this._actualSelectedObjectId, this._z,
      )
      this._sceneObjects[`${this._actualSelectedObjectId}|${position.toString()}`] = sceneObject
   }
}
