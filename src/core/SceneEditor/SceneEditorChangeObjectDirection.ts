export enum SceneEditorChangeObjectDirection {
   DOWN = -1,
   UP = 1,
}
