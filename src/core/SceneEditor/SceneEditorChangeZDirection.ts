export enum SceneEditorChangeZDirection {
   DOWN = -1,
   UP = 1,
}
