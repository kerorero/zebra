export enum SceneEditorModes {
   NORMAL = "normal",
   COLLISION = "collision",
   CAMERA = "camera",
}
