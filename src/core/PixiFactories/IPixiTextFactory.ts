import * as PIXI from "pixi.js"

export interface IPixiTextFactory {
   create: (text: string, style: object) => PIXI.Text
}
