import * as PIXI from "pixi.js"
import { IPixiApplicationFactory } from "./IPixiApplicationFactory"

export class PixiApplicationFactory implements IPixiApplicationFactory {
   public create(obj: object) {
      return new PIXI.Application(obj)
   }
}
