import * as PIXI from "pixi.js"

import { IPixiPointFactory } from "./IPixiPointFactory"

export class PixiPointFactory implements IPixiPointFactory {
   public create(x: number, y: number) {
      return new PIXI.Point(x, y)
   }
}
