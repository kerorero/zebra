import * as PIXI from "pixi.js"
import { IPixiGraphicsFactory } from "./IPixiGraphicsFactory"

export class PixiGraphicsFactory implements IPixiGraphicsFactory {
   public create() {
      return new PIXI.Graphics()
   }
}
