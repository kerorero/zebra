import * as PIXI from "pixi.js"

export interface IPixiGraphicsFactory  {
   create: () => PIXI.Graphics
}
