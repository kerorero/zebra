import * as PIXI from "pixi.js"
import { IPixiTextureFactory } from "./IPixiTextureFactory"

export class PixiTextureFactory implements IPixiTextureFactory {
   public create(
      baseTexture: PIXI.BaseTexture,
      frame?: PIXI.Rectangle,
      orig?: PIXI.Rectangle,
      trim?: PIXI.Rectangle,
      rotate?: number,
      anchor?: PIXI.Point) {
      return new PIXI.Texture(baseTexture, frame, orig, trim, rotate, anchor)
   }
}
