import * as PIXI from "pixi.js"

export interface IPixiApplicationFactory {
   create: (obj: object) => PIXI.Application
}
