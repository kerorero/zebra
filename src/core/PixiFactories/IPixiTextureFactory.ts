import * as PIXI from "pixi.js"

export interface IPixiTextureFactory {
   create: (
      baseTexture: PIXI.BaseTexture,
      frame?: PIXI.Rectangle,
      orig?: PIXI.Rectangle,
      trim?: PIXI.Rectangle,
      rotate?: number,
      anchor?: PIXI.Point,
   ) => PIXI.Texture
}
