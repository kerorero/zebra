import * as PIXI from "pixi.js"
import { IPixiSpriteFactory } from "./IPixiSpriteFactory"
import { IPixiTextFactory } from "./IPixiTextFactory"

export class PixiTextFactory implements IPixiTextFactory {
   public create(text: string, config: object) {
      return new PIXI.Text(text, config)
   }
}
