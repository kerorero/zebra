import * as PIXI from "pixi.js"

export interface IPixiRectangleFactory {
   create: (x: number, y: number, width: number, height: number) => PIXI.Rectangle
}
