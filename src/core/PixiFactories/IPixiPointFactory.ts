import * as PIXI from "pixi.js"

export interface IPixiPointFactory {
   create: (x: number, y: number) => PIXI.Point
}
