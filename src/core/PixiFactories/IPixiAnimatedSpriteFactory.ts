import * as PIXI from "pixi.js"

export interface IPixiAnimatedSpriteFactory {
   create: (
      textures: PIXI.Texture[] | PIXI.AnimatedSprite.FrameObject[],
      autoUpdate?: boolean,
   ) => PIXI.AnimatedSprite
}
