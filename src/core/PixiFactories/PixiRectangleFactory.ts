import * as PIXI from "pixi.js"
import { IPixiRectangleFactory } from "./IPixiRectangleFactory"

export class PixiRectangleFactory implements IPixiRectangleFactory {
   public create(x: number, y: number, width: number, height: number) {
      return new PIXI.Rectangle(x, y, width, height)
   }
}
