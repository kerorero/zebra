import "App.less"
import { App } from "core/App/App"
import { AppSetting } from "core/AppSetting/AppSetting"
import { Camera } from "core/Camera/Camera"
import { ICamera } from "core/Camera/ICamera"
import { StaticCameraPointCalculator } from "core/Camera/StaticCameraPointCalculator"
import { DialogResponseFunctionName } from "core/Dialog/DialogResponseFunctionName"
import { DialogManager } from "core/DialogManager/DialogManager"
import { DialogResponseBackToDialogueFunction } from "core/DialogManager/Functions/DialogResponseBackToDialogueFunction"
import { DialogResponseCancelFunction } from "core/DialogManager/Functions/DialogResponseCancelFunction"
import { NormalNPCFactory } from "core/Entity/NormalNPC/NormalNPCFactory"
import { Player } from "core/Entity/Player/Player"
import { EntitySpriteFactory } from "core/EntitySprite/EntitySpriteFactory"
import { KeyboardManager } from "core/KeyboardManager/KeyboardManager"
import { MouseManager } from "core/MouseManager/MouseManager"
import { NPCManager } from "core/NPCManager/NPCManager"
import { AnimatedSpriteManager } from "core/ObjectManagers/AnimatedSpriteManager"
import { SpriteManager } from "core/ObjectManagers/SpriteManager"
import { SpritesheetManager } from "core/ObjectManagers/SpritesheetManager"
import { PixiAnimatedSpriteFactory } from "core/PixiFactories/PixiAnimatedSpriteFactory"
import { PixiApplicationFactory } from "core/PixiFactories/PixiApplicationFactory"
import { PixiContainerFactory } from "core/PixiFactories/PixiContainerFactory"
import { PixiGraphicsFactory } from "core/PixiFactories/PixiGraphicsFactory"
import { PixiPointFactory } from "core/PixiFactories/PixiPointFactory"
import { PixiRectangleFactory } from "core/PixiFactories/PixiRectangleFactory"
import { PixiSpriteFactory } from "core/PixiFactories/PixiSpriteFactory"
import { PixiTextFactory } from "core/PixiFactories/PixiTextFactory"
import { PixiTextureFactory } from "core/PixiFactories/PixiTextureFactory"
import { ResourcesLoader } from "core/ResourcesLoader/ResourcesLoader"
import { ConfigSceneFactory } from "core/Scene/ConfigSceneFactory"
import { DevScene } from "core/Scene/DevScene"
import { SceneObjectFactory } from "core/Scene/SceneObjectFactory"
import { BasicTestScene } from "core/Scene/tests/BasicTestScene"
import { SceneEditor } from "core/SceneEditor/SceneEditor"
import { SceneEditorChooseObjectWindow } from "core/Window/SceneEditorChooseObjectWindow"
import { ScreenPositionFactory } from "core/WorldPositionFactory/ScreenPositionFactory"
import { WorldPositionFactory } from "core/WorldPositionFactory/WorldPositionFactory"
import $ from "jquery"
import Cookies from "js-cookie"
import { Random } from "random-js"

const random = new Random()
const pixiContainerFactory = new PixiContainerFactory()
const pixiApplicationFactory = new PixiApplicationFactory()
const pixiSpriteFactory = new PixiSpriteFactory()
const pixiTextureFactory = new PixiTextureFactory()
const pixiRectangleFactory = new PixiRectangleFactory()
const pixiAnimatedSpriteFactory = new PixiAnimatedSpriteFactory()
const pixiGraphicsFactory = new PixiGraphicsFactory()
const pixiTextFactory = new PixiTextFactory()
const appSetting = new AppSetting()
const spritesheetManager = new SpritesheetManager(pixiTextureFactory, pixiRectangleFactory)
const animatedSpriteManager = new AnimatedSpriteManager(pixiAnimatedSpriteFactory)
const entitySpriteFactory = new EntitySpriteFactory()
const configSceneFactory = new ConfigSceneFactory()
const sceneObjectFactory = new SceneObjectFactory()
const normalNpcFactory = new NormalNPCFactory(entitySpriteFactory, null, null, random)
const npcManager = new NPCManager(normalNpcFactory)
const spriteManager = new SpriteManager(pixiSpriteFactory)
const resourcesLoader = new ResourcesLoader(spritesheetManager, animatedSpriteManager, spriteManager, npcManager)
appSetting.mapBackgroundColor = 0x212121
appSetting.mainDiv = ".main"
appSetting.worldHeight = $("body").height()
appSetting.worldWidth = $(appSetting.mainDiv).width()
appSetting.sceneBuilder = true
appSetting.mapSize = 16

const worldPositionFactory = new WorldPositionFactory()
const screenPositionFactory = new ScreenPositionFactory()
const app = new App(
   pixiApplicationFactory,
   appSetting, resourcesLoader,
)
app.prepare()
const staticCameraPointCalculator = new StaticCameraPointCalculator(screenPositionFactory, appSetting)
const knownDialogueFunctions = {}
knownDialogueFunctions[DialogResponseFunctionName.CANCEL] = new DialogResponseCancelFunction(app.stage)
knownDialogueFunctions[DialogResponseFunctionName.BACK_TO] = new DialogResponseBackToDialogueFunction()
const dialogManager = new DialogManager(
   app.stage, null, staticCameraPointCalculator, pixiTextFactory, screenPositionFactory, knownDialogueFunctions,
)
normalNpcFactory.stage = app.stage
normalNpcFactory.dialogManager = dialogManager
const renderer: PIXI.Renderer = app.renderer
let devScene = null

if (Cookies.get("TEST_SCENE") && Cookies.get("TEST_SCENE").length > 0) {
   const scenesForTests = {
      BasicTest: new BasicTestScene(
         app.stage, pixiSpriteFactory, pixiRectangleFactory, appSetting, worldPositionFactory,
      ),
   }
   devScene = scenesForTests[Cookies.get("TEST_SCENE")]
} else {
   devScene = new DevScene(app.stage, pixiSpriteFactory, pixiRectangleFactory, appSetting, worldPositionFactory)
}

const keyboardManager = new KeyboardManager(app.stage, appSetting)
const camera: ICamera = new Camera(pixiRectangleFactory, appSetting, app.stage, worldPositionFactory)
dialogManager.camera = camera
const player = new Player(
   worldPositionFactory.create(1056, 310),
   entitySpriteFactory, app.stage, devScene,
   appSetting, camera, pixiRectangleFactory, random,
)
const sceneEditor = new SceneEditor(
   app.stage, pixiSpriteFactory, appSetting,
   configSceneFactory, sceneObjectFactory, worldPositionFactory, devScene, pixiTextFactory, camera,
)
const sceneEditorChooseObjectWindow = new SceneEditorChooseObjectWindow(
   app.stage, pixiSpriteFactory, pixiGraphicsFactory, appSetting, renderer, screenPositionFactory,
)
const mouseManager = new MouseManager(app.stage, appSetting, worldPositionFactory, screenPositionFactory, camera)

app.init()
